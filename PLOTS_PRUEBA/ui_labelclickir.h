/********************************************************************************
** Form generated from reading UI file 'labelclickir.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LABELCLICKIR_H
#define UI_LABELCLICKIR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_labelclickIR
{
public:
    QHBoxLayout *horizontalLayout;
    QLabel *label;

    void setupUi(QWidget *labelclickIR)
    {
        if (labelclickIR->objectName().isEmpty())
            labelclickIR->setObjectName(QStringLiteral("labelclickIR"));
        labelclickIR->resize(400, 300);
        horizontalLayout = new QHBoxLayout(labelclickIR);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(labelclickIR);
        label->setObjectName(QStringLiteral("label"));
        label->setScaledContents(true);

        horizontalLayout->addWidget(label);


        retranslateUi(labelclickIR);

        QMetaObject::connectSlotsByName(labelclickIR);
    } // setupUi

    void retranslateUi(QWidget *labelclickIR)
    {
        labelclickIR->setWindowTitle(QApplication::translate("labelclickIR", "Form", nullptr));
        label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class labelclickIR: public Ui_labelclickIR {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LABELCLICKIR_H
