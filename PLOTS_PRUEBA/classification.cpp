#include "classification.h"
#include "math.h"
#include <qpoint.h>
#include <QFileDialog>
#include <QDebug>
#include <QImage>
#include <QTime>
#include <QElapsedTimer>
#include <stdint.h>
#include "math.h"
#include <QThread>
#include <QLabel>
#include <QTextEdit>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <QMessageBox>


using namespace cv;
using namespace std;

#define number_of_frames_input_video 1000-2

QString classification(QList<float> lesion, QList<float> skin, QString algorithm)
{
    qDebug() << "Classification";
    int c = 0;
    float param[5];
    float frameRate = 60;
    float t[number_of_frames_input_video];

    fitData(lesion, param);

    QList<float> data_model;
   // float data_model[number_of_frames_input_video];

    for (int i = 0 ; i < number_of_frames_input_video; i++)
    {
        t[i] = (1/frameRate) * i;
    }

    for (int i = 0; i < number_of_frames_input_video; i++)
    {
        data_model.append(param[0] +param[1] * exp(param[2] * t[i]) + param[3] * exp(param[4] * t[i]));
    }

    QFile file2("/home/silvana/Documentos/data_model.txt");

    if (!file2.open(QIODevice::WriteOnly | QIODevice::Text))
        return "Data model not found";
    QTextStream out(&file2);


    for (int i = 0 ; i < number_of_frames_input_video; i++)
    {
        out << data_model.at(i) << ",";
        out << "\n";
    }

    float T;
    float i0, j0;

    if (algorithm == "Euclidean")
    {
        qDebug() << "Classification algorithm: Euclidean distance";
        for (int i = 0; i < lesion.size(); i++)
        {
            i0 = lesion.at(i);
            j0 = skin.at(i);

            T += sqrt((i0 - j0)*(i0 - j0));

        }
        float thresh = T/((float) lesion.size()- 1);
        qDebug() << "Euclidean distance between curves: " << thresh;

        if (thresh > 0.238)
        {
            //ui->textEdit->setText("Cancer alert!");
            qDebug() << "Cancer alert!";
            return "Cancer alert!";
        }
        else
        {
            //ui->textEdit->setText("You're fine, go home, be happy. Have a beer, eat some chocolate.");
            qDebug() << "You're fine, go home, be happy. Have a beer, eat some chocolate.";
            return "You're fine, go home, be happy. Have a beer, eat some chocolate.";
        }
    }
    else if (algorithm == "CommTheory01")
    {
        qDebug() << "Classification algorithm: Communication theory";
        float thresh = 13.05;  //Cambiar
        QVector<float> lam0 = getLambda("0");
        QVector<float> lam1 = getLambda("1");

        float psi0[number_of_frames_input_video][12] = {};
        getPsi(psi0, "0");
        float psi1[number_of_frames_input_video][12] = {};
        getPsi(psi1, "1");

        float Y0;   // = (float *) calloc (3300 * 12, sizeof(float));
        float Y1;   // = (float *) calloc (3300 * 12, sizeof(float));//3300???
        float Z = 0;
        float Z0, Z1;

        for (int j = 0; j < number_of_frames_input_video ; j++)
        {
            for (int i = 0; i < 12; i++) // n = 1 : numEIG
            {
                for (int k = 0; k < lesion.size(); k++)
                {
                    i0 = lesion.at(k);

                    Y0= psi0[j][i] * i0;
                    Y1= psi1[j][i] * i0;

                    Z0 = (Y0 * Y0) / lam0[i];
                    Z1 = (Y1 * Y1) / lam1[i];

                    Z +=  Z0 - Z1;
                }

            }
        }

        if (Z > thresh)
        {
            qDebug() << "Cancer alert!";
            return "Cancer alert";

        }
        else
        {
            qDebug() << "You're fine, go home, be happy. Have a beer, eat some chocolate.";
            return "You're fine, go home, be happy. Have a beer, eat some chocolate.";
        }

        qDebug() << "Test statistics Z = " << Z;

        return "";

    }
}

QVector<float> getLambda(QString lamval)
{
    QVector<float> lam (QVector<float>(12));

    QString fileName = "/home/silvana/Documentos/Códigos SGodoy/cdigosalfin/Codigos-DectionTheory/Codigos-DectionTheory/lam" + lamval + ".txt";
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0, "error", file.errorString());
    }

    QTextStream in(&file);
    int it = 0;
    while (it < 12)
    {
        bool ok = false;
        QString line = in.readLine();
        lam[it] = line.toFloat(&ok);
        //qDebug() << lam[it];
        it++;
    }

    return lam;

}

void getPsi(float psi[7000][12], QString psival)
{
    //float psi[3300][12] = {};//* psi;
    //psi =  (float *) calloc (3300 * 12, sizeof(float));

    QString fileName = "/home/silvana/Documentos/Códigos SGodoy/cdigosalfin/Codigos-DectionTheory/Codigos-DectionTheory/psi" + psival + ".txt";
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0, "error", file.errorString());
    }

    QTextStream in(&file);

    for (int row = 0; row < number_of_frames_input_video; row++)
    {
        QString line = in.readLine();
        QStringList val = line.split(' ');

        for (int col = 0; col < 12; col++)
        {
            bool ok = false;
            //psi[col] = val[col].toFloat(&ok);
            psi[row][col] = val[col].toFloat(&ok);
           // qDebug() << psi[row][col];

        }
    }

 //   return psi;
}

void fitData (QList<float> values, float param[5])
{
    int count = 0;

    float t[number_of_frames_input_video];
    float dt;
    float frameRate = 60;
   // QList<float> pixel;
    float kk[number_of_frames_input_video];
    int n = number_of_frames_input_video - 2;

    float matrix[n][5];

    //    QString fileName = "/home/silvana/Documentos/Códigos SGodoy/cdigosalfin/Codigos-DectionTheory/Codigos-DectionTheory/curves/lesion/Benign/Ordenados/2.txt";
    //    QFile file(fileName);
    //    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    //        QMessageBox::information(0, "error", file.errorString());
    //    }

    //    QTextStream in(&file);

    //   // qDebug() << in.readLine().split(",");

    //    while (!in.atEnd())
    //    {
    //        float tmp;
    //        float tmp2 = 0;
    //        QStringList var = in.readLine().split(",");

    //        QStringList::iterator it;

    //        for (it = var.begin(); it != var.end(); it++)
    //        {
    //            tmp2 += (* it).toFloat();
    //        }

    //        var.clear();
    ////        for (int i = 0 ; i < 2550; i++)
    ////        {
    ////            in >> tmp;
    ////            tmp2 += tmp;
    ////        }

    //        values.append(tmp2/2550);
    //    }

    dt = 1/frameRate;


    qDebug() << "fitData" << count++;
    //pixel = values;

    for (int i = 0; i < number_of_frames_input_video; i++)
    {
        kk[i] = i;
    }
    qDebug() << "fitData" << count++;
    float u[number_of_frames_input_video];
    float v[number_of_frames_input_video];
    qDebug() << "fitData" << count++;
    u[0] = values[0];
    v[0] = u[0];

    qDebug() << "fitData" << count++;
    for (int i = 1; i < number_of_frames_input_video; i++)
    {
        u[i] = u[i - 1] + values[i];
        v[i] = v[i - 1] + u[i];

        //qDebug() << i ;
    }

    qDebug() << "fitData" << count++;
    for (int i = 0; i < n; i++)
    {
        matrix[i][0] = u[i + 1];
        matrix[i][1] = v[i];
        matrix[i][2] = (1 - kk[i]) * kk[i]/2;
        matrix[i][3] = kk[i];
        matrix[i][4] = 1;
    }
    qDebug() << "fitData" << count++;

    float pixel[number_of_frames_input_video];
    qDebug() << "fitData" << count++;
    for (int i = 0; i < number_of_frames_input_video; i++)
    {
        pixel[i] = values.at(i);
    }
    qDebug() << "fitData" << count++;
    float q[5];
    Mat mat0 = Mat(n, 5, CV_32F, matrix);

    Mat pix0 = Mat(number_of_frames_input_video, 1, CV_32F, pixel);
    Mat q0 = Mat(5, 1, CV_32F, q);

    cv::solve(mat0, pix0, q0, cv::DECOMP_SVD);

    for (int i = 0; i < 5; i++)
    {
        q[i] = q0.at<float>(i);
    }

    float a, b ,c;

    a = -q[0];
    b = -q[1];
    c = -q[2];

    qDebug() << a << b << c;

    float coeffs[3];
    coeffs[0] = 1 - a + b;
    coeffs[1] = a - 2;
    coeffs[2] = 1 ;

    Mat coeffsCv = Mat(3, 1, CV_32F, coeffs);

    Mat rootsCv;
    float roots[5];

    cv::solvePoly(coeffsCv, rootsCv);


    for (int i = 0; i < 5; i++)
    {
        roots[i] = rootsCv.at<float>(i);
    }

    float jac[number_of_frames_input_video][2];

    for (int i = 0; i < number_of_frames_input_video; i ++)
    {
        jac[i][0] = pow(roots[0], kk[i]);
        jac[i][1] = pow(roots[2], kk[i]);
    }

    for (int i = 0 ; i < number_of_frames_input_video; i++)
    {
        pixel[i] = pixel[i] - c/b;
    }

    Mat jacCv = Mat(number_of_frames_input_video, 2, CV_32F, jac);
    Mat pixelCv = Mat(number_of_frames_input_video, 1, CV_32F, pixel);

    float sols[2];
    Mat solsCv = Mat(2, 1, CV_32F, sols);
    cv::solve(jacCv, pixelCv, solsCv, cv::DECOMP_SVD);

    sols[0] = solsCv.at<float>(0);
    sols[1] = solsCv.at<float>(1);

    qDebug() << sols[0] << sols[1];

    float alpha, beta0, lambda0, beta1, lambda1;

    alpha = c/b;
    beta0 = sols[0];
    lambda0 = log(roots[0])/dt;
    beta1 = sols[1];
    lambda1 = log(roots[2])/dt;

    param[0] = alpha;
    param[1] = beta0;
    param[2] = lambda0;
    param[3] = beta1;
    param[4] = lambda1;

    qDebug() << alpha << beta0 << lambda0 << beta1 << lambda1;

}
