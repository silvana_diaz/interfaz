#include "tauudp.h"
#include <QDebug>

#include <stdio.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <stddef.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

TauUDP::TauUDP()
{

}

void TauUDP::process()
{
    struct sockaddr_in sock_addr_listener;
    int socket_listener = socket ( AF_INET, SOCK_DGRAM, 0 );

    if ( socket_listener < 0 )
    {
        printf ( "UDP sender: Error creating socket\n" );
        return;
    }

    // Fill connection data
    sock_addr_listener.sin_family      = AF_INET;
    sock_addr_listener.sin_addr.s_addr = INADDR_ANY;
    sock_addr_listener.sin_port        = htons ( 1234 );
    int reuse_sock = 1;

    // Reuse port
    if ( setsockopt ( socket_listener, SOL_SOCKET, SO_REUSEPORT, &reuse_sock, sizeof ( int ) ) < 0 )
    {
        printf ( "UDP listener: Error reusing address\n" );
        close ( socket_listener );
        return;
    }

    // Binding socket
    if ( bind ( socket_listener, ( struct sockaddr * ) &sock_addr_listener, sizeof ( sock_addr_listener ) ) < 0 )
    {
        close ( socket_listener );
        printf ( "UDP listener: Error binding socket\n" );
        return;
    }

    uint8_t buffer [640*2+2];

    int lrow = 0;
    int row;

    while ( 1 )
    {

        recv ( socket_listener, (uint8_t *) buffer, 640*2+2, MSG_WAITALL );
        row = (quint8(buffer[1281]) << 8) | quint8(buffer[1280]);

        memcpy( FrameTau + row*640 , buffer, 640*2);

        if ( row == 511 )
        {
            emit newFrame(FrameTau);
        }
        if ( row != lrow + 1 && row != 0 ) qDebug() << "Error line";

//        printf("row %p %d  %d\n", FrameTau, row, lrow  );
        lrow = row;
    }

    emit finished();
}
