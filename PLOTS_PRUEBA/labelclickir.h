#ifndef LABELCLICKIR_H
#define LABELCLICKIR_H

#include <QWidget>
#include <QMouseEvent>
#include <QDebug>
#include <QPainter>

#include <QLabel>

namespace Ui {
class labelclickIR;
}

class labelclickIR : public QWidget
{
    Q_OBJECT

public:
    explicit labelclickIR(QWidget *parent = 0);
    ~labelclickIR();

private:
    Ui::labelclickIR *ui;
};

#endif // LABELCLICKIR_H
