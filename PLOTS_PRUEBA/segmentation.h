#ifndef SEGM_H
#define SEGM_H
#include <stdint.h>

#define ROWROI 117
#define COLROI 114
//#define SIZE_SPARSE  (ROWROI*COLROI-2)*(COLROI*2)

//#define CONST (ROWROI*COLROI-2)

#include <QDebug>
#include <QImage>
#include <QTime>
#include <stdint.h>

#define mini(X, Y) (((X) < (Y)) ? (X) : (Y))
#define maxi(X, Y) (((X) > (Y)) ? (X) : (Y))

//#define EPSILON     1e-5
//#define VARSIZE     ((ROWROI*COLROI*2)-(ROWROI+COLROI))
//#define VAR2SIZE    (ROWROI*COLROI) + 2*VARSIZE


struct sparse{
    int i;
    int j;
    float value;
};

void multithresholding(uint8_t *mat_otsu, uint8_t * mat, int row, int col, bool binary, int binaryChoice);
void greyMultithresholding(uint8_t *mat_otsu, uint8_t * mat, int row, int col);
//void RW(uint8_t * otsu_img, uchar mask_final[ROWROI][COLROI], int seeds[2]);
//void RW(uchar otsu_img[ROWROI][COLROI], uchar mask_final[ROWROI][COLROI], int seeds[2]);
void RW(uint8_t *otsu_img, uint8_t *mask_final, int seeds[2], int row, int col);
//void multithresholding2(uint8_t mat_otsu[ROWROI][COLROI], uint8_t I[ROWROI*COLROI][3]);
//void multithresholding(uint8_t  mat_otsu[ROWROI][COLROI], uint8_t mat[ROWROI][COLROI][3]);
void erode(uint8_t * mask, int miniCol, int miniRow, uint8_t * mask_erode, int SE);
void dilate(uint8_t *mask, int miniCol, int miniRow, uint8_t *mask_dilate, int SE);
int maxCorners(int a, int b, int c, int d);
int minCorners(int a, int b, int c, int d);

bool convolve2D(unsigned char* in, unsigned char* out, int dataSizeX, int dataSizeY,
                float* kernel, int kernelSizeX, int kernelSizeY);
void binarization(uint8_t * mat_otsu, uint8_t * mat, int row, int col, int optThresh);

#endif // SEGMENTATION_H
