#include "labelclick.h"
#include "ui_labelclick.h"
#include "mainwindow.h"
#include <QPen>
#include <QPainter>

labelclick::labelclick(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::labelclick)
{
    ui->setupUi(this);

    mousePressed = false;
    drawStarted = false;
    label = (ui->label);
}

labelclick::~labelclick()
{
    delete ui;
}

void labelclick::setPixmap(QPixmap pix)
{
    ui->label->setPixmap(pix);
}

void labelclick::mousePressEvent(QMouseEvent *event)
{
    if(enter < 4)
    {
        mousePressed = true;
        drawEnd = false;

        mRect.setTopLeft(event->pos());
        mRect.setBottomRight(event->pos());
        //beginR = event->pos();
        cornersEnd = false;
    }
    else
    {
        emit pressed(event->pos());
    }

    if (enter == 4)
        enter = 0;
}

void labelclick::mouseMoveEvent(QMouseEvent *event)
{
    if((event->type() == QEvent::MouseMove) && (enter < 4))
    {
        mRect.setBottomRight(event->pos());
    }
}

void labelclick::mouseReleaseEvent(QMouseEvent *event)
{
    qDebug()<< "~click" << enter;

    if(!otsuReady)
    {
        mousePressed = false;
        drawEnd = true;

        posRightDown[enter].setX(int(event->pos().x())*propw);
        posRightDown[enter].setY(int(event->pos().y())*proph);

        enter++;
        if (ir)
            flag = true;
        qDebug()<< "enter" << enter;
        update();
    }
    else
    {
        enter = 0 ;
        emit seeds(event->x(), event->y());
    }
}

const QPixmap & labelclick::Pixmap()
{
    return * ui->label->pixmap();
}

void labelclick::paintEvent(QPaintEvent *event)
{
    if ((ui->label->pixmap()) == NULL)
    {
        return;
    }

    QPixmap PIX = *(ui->label->pixmap());
    painter.begin(&PIX);

    if (mousePressed)
    {
        drawStarted = true;
    }
    else if (drawStarted && drawEnd && (enter < 5))
    {
        int w_i = this->width();
        int w_l = ui->label->pixmap()->width();
        float propw = float(w_l)/w_i;
        int h_i = this->height();
        int h_l = ui->label->pixmap()->height();
        float proph = float(h_l)/h_i;

        qDebug() << propw << proph;

        QPoint pos_l = mRect.topLeft();
        QPoint pos_r = mRect.bottomRight();

        qDebug() <<  "pos00" << pos_l << pos_r;

        pos_l.setX(int(pos_l.x()));//*propw));
        pos_l.setY(int(pos_l.y()));//*proph));
        pos_r.setX(int(pos_r.x()));//*propw));
        pos_r.setY(int(pos_r.y()));//*proph));

        qDebug() << "pos11" << pos_l << pos_r;

        posLeftUp[enter-1] = pos_l;
        posRightDown[enter-1] = pos_r;

        mRect.setTopLeft(pos_l);
        mRect.setBottomRight(pos_r);

        QPen pinkpen(QColor(180,0,180),2);
        painter.setPen(pinkpen);
        painter.drawRect(mRect);
        drawEnd = false;
        emit numcuad(enter);
      // painter.drawPixmap(0, 0, mPix);
    }

    if (enter == 4 && !ir)
    {
        cornersEnd = true;
        getCornerData(4);
        emit getCorners();

        enter = 0;
        painter.end();
        return;
    }
    if (ir && flag)
    {
     //   qDebug() << "IR enter" << enter;
        cornersEnd = true;
        getCornerData(1);

        painter.end();
        ui->label->setPixmap(PIX);

       // qDebug() << "EMIT GETCORNER";
        emit getCorners();

        PIX = *(ui->label->pixmap());
        painter.begin(&PIX);

        flag = false;

        if (enter == 4)
        {
            painter.end();
            return;
        }
    }

    painter.end();
    ui->label->setPixmap(PIX);
}

void labelclick::getCornerData(int it)
{
    if (ir)
    {
        int i = enter-1;
        posRightUp[i].setX(posRightDown[i].x());
        posRightUp[i].setY(posLeftUp[i].y());
        posLeftDown[i].setX(posLeftUp[i].x());
        posLeftDown[i].setY(posRightDown[i].y());
   //     qDebug() << "enter" << enter - 1;

//        qDebug() << "getCornerData" << posLeftDown[i].x() << posLeftDown[i].y();
    }
    else
    {
        for (int i = 0; i < it; i++)
        {
            posRightUp[i].setX(posRightDown[i].x());
            posRightUp[i].setY(posLeftUp[i].y());
            posLeftDown[i].setX(posLeftUp[i].x());
            posLeftDown[i].setY(posRightDown[i].y());
        }
    }
}

/*
#include "labelclick.h"
#include "ui_labelclick.h"
#include "mainwindow.h"

int w_i;
int w_l;

int h_i;
int h_l;

labelclick::labelclick(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::labelclick)
{
    ui->setupUi(this);

    mPix = QPixmap(640,480);
    mPix.fill(Qt::white);

    mousePressed = false;
    drawStarted = false;

    label = (ui->label);
}

labelclick::~labelclick()
{
    delete ui;
}

void labelclick::setPixmap(QPixmap pix)
{
    ui->label->setPixmap(pix);
}

void labelclick::mousePressEvent(QMouseEvent *event)
{
    mousePressed = true;
    drawEnd = false;

    mRect.setTopLeft(event->pos());
    mRect.setBottomRight(event->pos());

    cornersEnd = false;

    w_i = this->width();
    w_l = ui->label->pixmap()->width();
    propw = float(w_l)/w_i;
    h_i = this->height();
    h_l = ui->label->pixmap()->height();
    proph = float(h_l)/h_i;

    posLeftUp[enter].setX(int(event->pos().x()) * propw);
    posLeftUp[enter].setY(int(event->pos().y()) * proph);
}

void labelclick::mouseMoveEvent(QMouseEvent *event)
{
    if(event->type() == QEvent::MouseMove)
    {
        mRect.setBottomRight(event->pos());
    }
}

void labelclick::mouseReleaseEvent(QMouseEvent *event)
{
    mousePressed = false;
    drawEnd = true;

    posRightDown[enter].setX(int(event->pos().x())*propw);
    posRightDown[enter].setY(int(event->pos().y())*proph);

    //qDebug() << "Enter" << enter << ".."<<  posRightDown[enter];

    enter++;
    if (enter == 4 && !ir)
    {
        qDebug() << "enter 4";
        cornersEnd = true;
        getCornerData(4);
        emit getCorners();

        //if (ir)
            enter = 0;
        return;
    }
    if (ir)
    {
        qDebug() << "IR enter";
        cornersEnd = true;
        getCornerData(1);
        emit getCorners();

        if (enter == 4)
            return;
    }
    update();
}

void labelclick::paintEvent(QPaintEvent *event)
{
    if ((ui->label->pixmap()) == NULL)
    {
        return;
    }

    QPixmap PIX = *(ui->label->pixmap());
    painter.begin(&PIX);

    if (mousePressed)
    {
        //painter.drawPixmap(0,0,mPix);
        //painter.drawRect(mRect);
        drawStarted = true;
    }
    else if (drawStarted && drawEnd)
    {
        QPoint pos_l = mRect.topLeft();
        QPoint pos_r = mRect.bottomRight();
        pos_l.setX(int(pos_l.x())*propw);
        pos_l.setY(int(pos_l.y()*proph));
        pos_r.setX(int(pos_r.x()*propw));
        pos_r.setY(int(pos_r.y()*proph));
        mRect.setTopLeft(pos_l);
        mRect.setBottomRight(pos_r);

        QPen pinkpen(QColor(180,0,180),2);
        painter.setPen(pinkpen);
        painter.drawRect(mRect);
        drawEnd = false;
        emit numcuad(enter);
      // painter.drawPixmap(0, 0, mPix);
    }

    painter.end();
    ui->label->setPixmap(PIX);
}


void labelclick::getCornerData(int it)
{
    for (int i = 0; i < it; i++)
    {
        posRightUp[i].setX(posRightDown[i].x());
        posRightUp[i].setY(posLeftUp[i].y());
        posLeftDown[i].setX(posLeftUp[i].x());
        posLeftDown[i].setY(posRightDown[i].y());
    }
    if (ir)
    {
        int i = enter-1;
        posRightUp[i].setX(posRightDown[i].x());
        posRightUp[i].setY(posLeftUp[i].y());
        posLeftDown[i].setX(posLeftUp[i].x());
        posLeftDown[i].setY(posRightDown[i].y());
    }
}
*/

