#ifndef TEST_H
#define TEST_H

#include <QMainWindow>
//#include <QUdpSocket>

namespace Ui {
class Test;
}

class Test : public QMainWindow
{
    Q_OBJECT

public:
    explicit Test(QWidget *parent = 0);
    ~Test();

public slots:
    void newFrame (quint8 *frame);


private:
    Ui::Test *ui;
    int line_counter;
};

#endif // TEST_H
