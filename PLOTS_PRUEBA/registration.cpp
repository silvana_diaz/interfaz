#include "registration.h"
#include "math.h"
#include <qdebug.h>
//#include <arm_neon.h>
#include <qpoint.h>

void tform_estim(float theta[8], float eta[2], CORNERS * fix, CORNERS * mov)
{
    CORNERS_f err;
    float dtheta[8] = {0, 0, 0, 0, 0, 0, 0, 0};

    int i;
    // obtener el error en las coordenadas transformadas
    // err = y-A*theta
    tform_error(theta, fix, mov, &err);

    // calcular A'*err
    for( i = 0; i < 4; i++ ){
        dtheta[0] += mov->c[i].x0*err.c[i].x0;
        dtheta[1] += mov->c[i].x1*err.c[i].x0;
        dtheta[2] += err.c[i].x0;

        dtheta[3] += mov->c[i].x0*err.c[i].x1;
        dtheta[4] += mov->c[i].x1*err.c[i].x1;
        dtheta[5] += err.c[i].x1;

        dtheta[6] += -mov->c[i].x0*fix->c[i].x0*err.c[i].x0;
        dtheta[7] += -mov->c[i].x1*fix->c[i].x0*err.c[i].x0;

        dtheta[6] += -mov->c[i].x0*fix->c[i].x1*err.c[i].x1;
        dtheta[7] += -mov->c[i].x1*fix->c[i].x1*err.c[i].x1;
    }

    // actualizar theta
    theta[0] += eta[0]*dtheta[0]; //rotacion - escalamiento
    theta[1] += eta[0]*dtheta[1]; //rotacion - shear
    theta[2] += eta[1]*dtheta[2]; // traslacion - shear
    theta[3] += eta[0]*dtheta[3]; // rotacion - shear
    theta[4] += eta[0]*dtheta[4]; // rotacion - escalamiento
    theta[5] += eta[1]*dtheta[5]; // traslación

    theta[6] += eta[2]*dtheta[6]; // proyección@%$&·#
    theta[7] += eta[2]*dtheta[7];

//    theta[0] =  0.8692;
//    theta[1] =  0.0257;
//    theta[2] =  2.6222;
//    theta[3] = -0.0434;
//    theta[4] =  0.8817;
//    theta[5] = 92.3376;
//    theta[6] =  0.0001;
//    theta[7] = -0.0001;
}

int tform_error(float theta[8], CORNERS *fix, CORNERS *mov, CORNERS_f *err)
{
    if( !theta || !fix || !mov || !err )
        return -1;

    // transformar las coordenadas
    tform_corners(theta, fix, err);
    // calcular el error
    for( int i = 0; i < 4; i++ ){
        err->c[i].x0 = mov->c[i].x0 - err->c[i].x0;
        err->c[i].x1 = mov->c[i].x1 - err->c[i].x1;

      //  qDebug()<< "err: " << err->c[i].x0 << " - " << err->c[i].x1;
    }


    return 0;
}

float interp2(uint16_t *src, float x0, float x1) //uint8_t interp2(uint8_t *src, float x0, float x1)
{
    int x0_i, x1_i;
    uint16_t p00, p01, p10, p11;
    int i;
    float p1, p2;
    float f0, f1;

    // coordenadas quedan fuera de la imagen + borde de 1px
    if( x0 <= -1 || x1 <= -1 || x0 >= 640 || x1 >= 480 )
        return 0;

    return src[int(x1)*FRAME_TAU_WIDTH + int(x0)];

    // obtener parte entera
    x1_i = truncf( x1 );

    // parte fraccional de las coordenadas
    x0_i = truncf( x0 );
    f0 = fabs( x0 - x0_i );
    f1 = fabs( x1 - x1_i );

    i = x1_i*640 + x0_i;

    p00 = src[i];
    if( x0_i >= 640-1 ){
        p01 = 0;
    }else{
        p01 = src[i+1];
    }
    i += 640;
    if ( x1_i >= 480-1 ){
        p10 = 0;
        p11 = 0;
    }else{
        p10 = src[i];
        p11 = src[i+1];
    }

    p1 = p00*(1.0-f1) + p10*f1;
    p2 = p01*(1.0-f1) + p11*f1;

    return p1*(1.0-f0) + p2*f0;
}

uint8_t interp2TEST(uint8_t *src, float x0, float x1)
{
    int x0_i, x1_i;
    uint16_t p00, p01, p10, p11;
    int i;
    float p1, p2;
    float f0, f1;

    // coordenadas quedan fuera de la imagen + borde de 1px
    if( x0 <= -1 || x1 <= -1 || x0 >= 640 || x1 >= 480 )
        return 0;

    return src[int(x1)*FRAME_TAU_WIDTH + int(x0)];

    // obtener parte entera
    x1_i = truncf( x1 );

    // parte fraccional de las coordenadas
    x0_i = truncf( x0 );
    f0 = fabs( x0 - x0_i );
    f1 = fabs( x1 - x1_i );

    i = x1_i*640 + x0_i;

    p00 = src[i];
    if( x0_i >= 640-1 ){
        p01 = 0;
    }else{
        p01 = src[i+1];
    }
    i += 640;
    if ( x1_i >= 480-1 ){
        p10 = 0;
        p11 = 0;
    }else{
        p10 = src[i];
        p11 = src[i+1];
    }

    p1 = p00*(1.0-f1) + p10*f1;
    p2 = p01*(1.0-f1) + p11*f1;

    return p1*(1.0-f0) + p2*f0;
}



// aplicar la transformacion con los parametros en theta
// a los puntos en c. Almacenar resultado en dest
int tform_corners(float theta[8], CORNERS *c, CORNERS_f *dest)
{
    if( !dest )
        return -1;
    float b;
    for( int i = 0; i < 4; i++ ){
        b = (theta[6]*c->c[i].x0 + theta[7]*c->c[i].x1 + 1);
        dest->c[i].x0 = (theta[0]*c->c[i].x0 + theta[1]*c->c[i].x1 + theta[2])/b;
        dest->c[i].x1 = (theta[3]*c->c[i].x0 + theta[4]*c->c[i].x1 + theta[5])/b;
    }

    return 0;
}

void apply_proy_tform_line(uint8_t * src, uint8_t *dst, COORDS * cm_src, COORDS * cm_dst, float theta[8], int i )
{
    float x0, x1;
    int j, k = i*FRAME_TAU_WIDTH;

   // i -= cm_dst->x1;

    for ( j = 0; j < 640; j++ )
    {
        x0 = ((j*theta[0] + i*theta[1] + theta[2])/(j*theta[6] + i*theta[7] + 1)); //+ cm_src->x0;
        x1 = ((j*theta[3] + i*theta[4] + theta[5])/(j*theta[6] + i*theta[7] + 1)); //+ cm_src->x1;

        dst[k++] = (interp2TEST(src, x0, x1) >> 2) & 0xFF;


        if (i == cm_dst->x0 && j == cm_dst->x1)
            qDebug() << i << j << x0 << x1 << cm_src->x0 << cm_src->x1;

    }


}

float apply_proy_tform_mask (uint16_t *src, uint8_t *dst, bool lesion, float theta[8], QList<QPoint> * mask, CORNERS * corners_proy, CORNERS c_assoc)
// QVector<float> apply_proy_tform_mask (uint16_t *src, uint8_t *dst, bool lesion, float theta[8], QList<QPoint> * mask)
{
    int x0, x1, k;

    float i, j;
    float temp_val_acum = 0;

    float m = 145.9576361191297; //35.439499348498224;
    float b = 7004.579701540508; //-332.1175072423537;

    float avg_temp = 0;
    float dstTemp;

    uint16_t max = src[0];
    uint16_t min = src[0];

    for (int i = 0; i < FRAME_TAU_HEIGHT*FRAME_TAU_WIDTH*2; i++)
    {
        if (src[i] > max)
        {
          max = src[i];
        }
        else if (src[i] < min)
        {
          min = src[i];
        }
    }

//    min = 8098;
//    max = 10831;

    int mask_len = mask->size();
    QVector<float> temp_val(mask_len);

    //qDebug() << "size" << mask_len;
    int c = 0;
    QList<QPoint>::iterator maskItinit = mask->begin();
    QList<QPoint>::iterator maskItend = mask->end();

    for (; maskItinit != maskItend; ++maskItinit)
    {
        i = (* maskItinit).x();
        j = (* maskItinit).y();

        // Destination coordinate
        k = j*FRAME_TAU_WIDTH + i;

        i = (* maskItinit).y();
        j = (* maskItinit).x();

        x0 = ((j*theta[0] + i*theta[1] + theta[2])/(j*theta[6] + i*theta[7] + 1));
        x1 = ((j*theta[3] + i*theta[4] + theta[5])/(j*theta[6] + i*theta[7] + 1));

        // Store on destination matrix
        dstTemp = (interp2(src, x0, x1));

        //temp_val[c] = (dstTemp - b ) / ((float) m );
        //c = c+1;
        temp_val_acum += (dstTemp - b ) / ((float) m );
        if (lesion)
            dst[k] = (dstTemp - min)/float(max - min) * 255;
        //dst[k] = (uint8_t) dstTemp;
    }

    for (int i = 0; i < 4; i ++)
    {
        corners_proy->c[i].x0 = ((c_assoc.c[i].x0*theta[0] + c_assoc.c[i].x1*theta[1] + theta[2])/(c_assoc.c[i].x0*theta[6] + c_assoc.c[i].x1*theta[7] + 1));
        corners_proy->c[i].x1 = ((c_assoc.c[i].x0*theta[3] + c_assoc.c[i].x1*theta[4] + theta[5])/(c_assoc.c[i].x0*theta[6] + c_assoc.c[i].x1*theta[7] + 1));

        //qDebug() << "ProyT" << corners_proy.c[i].x0 << corners_proy.c[i].x1;
    }

    avg_temp = temp_val_acum / (float) mask_len;
   // return temp_val;
    return avg_temp;
}

float apply_proy_firstframe (uint16_t *src, uint8_t *dst, bool lesion, float theta[8], QList<QPoint> * mask, QList<QPoint> * maskOut, float avgTempLesionFirstFrame)
{
    int x0, x1, k;
    float i, j;
    float temp_val_acum = 0;
    float avg_temp = 0;
    float dstTemp;

    uint16_t max = src[0];
    uint16_t min = src[0];

    for (int i = 0; i < FRAME_TAU_HEIGHT*FRAME_TAU_WIDTH*2; i++)
    {
        if (src[i] > max)
        {
          max = src[i];
        }
        else if (src[i] < min)
        {
          min = src[i];
        }
    }
    int mask_len = mask->size();


    float m = 145.9576361191297; //35.439499348498224;
    float b = 7004.579701540508; //-332.1175072423537;

    int c = 0;
    QList<QPoint>::iterator maskItinit = mask->begin();
    QList<QPoint>::iterator maskItend = mask->end();

    for (; maskItinit != maskItend; ++maskItinit)
    {
        i = (* maskItinit).x();
        j = (* maskItinit).y();

        // Destination coordinate
        k = j*FRAME_TAU_WIDTH + i;

        i = (* maskItinit).y();
        j = (* maskItinit).x();

        x0 = ((j*theta[0] + i*theta[1] + theta[2])/(j*theta[6] + i*theta[7] + 1));
        x1 = ((j*theta[3] + i*theta[4] + theta[5])/(j*theta[6] + i*theta[7] + 1));

        // Store on destination matrix
        dstTemp = (interp2(src, x0, x1));
  //      if (firstFrame)
        dstTemp = (dstTemp - b ) / ((float) m );

        if (dstTemp > avgTempLesionFirstFrame - 0.2 && dstTemp < avgTempLesionFirstFrame + 0.2)
        {
            temp_val_acum += dstTemp;
            maskOut->append(QPoint(j,i));
            mask_len = c;
            c++;
        }
        else
            temp_val_acum += 0;
        if (lesion)
            dst[k] = (dstTemp - min)/float(max - min) * 255;
        //dst[k] = (uint8_t) dstTemp;
    }

    avg_temp = temp_val_acum / (float) mask_len;
    return avg_temp;
}

//void apply_proy_Neon(uint8_t * src, uint8_t *dst, COORDS * cm_src, COORDS * cm_dst, float theta[8], int i)
//{
//    int x0, x1;
//    int p1 = cm_src->x0;
//    int p2 = cm_src->x1;
//    int j, k;
//    k = i*FRAME_TAU_WIDTH;

//    int k_temp = 0;

//    i -= cm_dst->x1;

//    float a1 = i*theta[1] + theta[2];
//    float a2 = i*theta[4] + theta[5];
//    float a3 = i*theta[7];
//    float a4;
//    float a5 = a3 + 1;

//    float t0 = theta[0];
//    float t3 = theta[3];
//    float t6 = theta[6];

//    int initial_x0 = -cm_dst->x0;
//    int initial_x0fin = initial_x0 + 640;

//    a4 = (initial_x0*t6 + a3 + 1);

//   /* for( j = initial_x0; j < initial_x0fin; ++j ) {
//        a4 += t6;


//        x0 = ((j*t0 + a1) / a4) + p1;
//        x1 = ((j*t3 + a2) / a4) + p2;

//       // dst[k++] = (interp2(src, x0, x1) >> 2) & 0xFF;

//        if( x0 <= -1 || x1 <= -1 || x0 >= 640 || x1 >= 512 )
//            dst[k++] = 0;
//        else
//            dst[k++] = (src[x1*FRAME_TAU_WIDTH + x0] >> 2) & 0xFF;
//        //return src[int(x1)*FRAME_TAU_WIDTH + int(x0)];
//    }*/
////return;
////    x0 = j*(theta[0]/a4) + p1;
////    x1 = j*(theta[3]/a4) + p2;

////    float32x2x4_t vect [4] = {};
////    float32_t vect [4];
////    float32_t t1 [4];

//    float32x4_t res, op1, op2;
//    uint32x4_t ires;
//   // float values1[5];
//    uint32_t ivalues[5];
//  //  uint16_t ivalues16[5];

//    float32x4_t tm1 = vmovq_n_f32(a5);
//    float32x4_t tm2 = vmovq_n_f32(a1);
//    float32x4_t tm3 = vmovq_n_f32(a2);
//    float32x4_t c_1 = vmovq_n_f32(p1);
//    float32x4_t c_2 = vmovq_n_f32(p2);

//    float32x4_t lim_topleft = vmovq_n_f32(-1);
//    float32x4_t lim_right = vmovq_n_f32(640);
//    float32x4_t lim_bot = vmovq_n_f32(480);
//    uint32x4_t zeros = vmovq_n_u32(0);

//    uint32_t height = 640;

//    uint8_t dst_tmp[FRAME_TAU_WIDTH];

//   for( j = initial_x0; j < initial_x0fin; j = j+4 ) {

//        float32x4_t v = { 1.0+j, 2.0+j, 3.0+j, 4.0+j };

//        res = vmlaq_n_f32( tm1, v,t6 ); //j*theta[6] + a5
//        res = vrecpeq_f32(res);

////        (j*theta[0] + a1)
//        op1 = vmlaq_n_f32( tm2, v, t0 ); //j*theta[0] + a1
////        (j*theta[0] + a1) * values[u] + p1;
//        op1 = vmlaq_f32( c_1, op1, res ); //(j*theta[0] + a1) *


////        (j*theta[3] + a2)
//        op2 = vmlaq_n_f32( tm3, v, t3 );
////         j*theta[3] + a2) * values[u] + p2;
//        op2 = vmlaq_f32( c_2, op2, res );

////        if( x0 <= -1 || x1 <= -1 || x0 >= 640 || x1 >= 512 )
////              return 0;
//        uint32x4_t m1 = vcleq_f32( op1, lim_topleft );
//        uint32x4_t m2 = vcleq_f32( op2, lim_topleft );

//        uint32x4_t m3 = vcgeq_f32( op1, lim_right );
//        uint32x4_t m4 = vcgeq_f32( op2, lim_bot );

//        uint32x4_t n1 = vorrq_u32(m1, m2);
//        uint32x4_t n2 = vorrq_u32(m3, m4);
//        uint32x4_t or1 = vorrq_u32(n1, n2);

//        uint32x4_t iop1 = vcvtq_u32_f32(op1);
//        uint32x4_t iop2 = vcvtq_u32_f32(op2);

//        ires = vmlaq_n_u32(iop1, iop2, float32_t(FRAME_TAU_WIDTH)); //int(x1)*FRAME_TAU_WIDTH + int(x0)

////        x1_i = truncf( x1 );
////        float32x4_t fop1 = vcvtq_f32_u32(iop1);
////        float32x4_t fop2 = vcvtq_f32_u32(iop2);
////        //x0-x0_i
////        float32x4_t f0 = vsubq_f32(op1,fop1);
////        float32x4_t f1 = vsubq_f32(op2,fop2);

////        uint32x4_t i_f = vmlaq_n_u32(iop2, iop1, height); // i = x1_i*640 + x0_i;
////        uint32x4_t p00; // p00 = src[i]

////        p00 = src[i_f];




//        ires = vbslq_u32(or1, zeros, ires);
//        vst1q_u32(ivalues, ires);



////        ires  = vshrq_n_u32(ires, 2);
////        uint16x4_t ires_16 = vqmovun_u32(ires);

////        vst1_u16 ( &dst[k], ires_16);

////        qDebug () << dst[0] << dst[1] << dst[2] << dst[3];

////        k = k + 4;
////        x0 = (j*theta[0] + a1);
////        x0 = x0 / a4 + p1;
////        x1 = (j*theta[3] + a2) / a4 + p2;

////        a4 = (j*theta[6] + a5);

////        if( x0 <= -1 || x1 <= -1 || x0 >= 640 || x1 >= 512 )
////            return 0;

////        return src[int(x1)*FRAME_TAU_WIDTH + int(x0)];

//        for (int u = 0; u < 4; ++u )
//        {
//            dst_tmp[k_temp++] = (src[ivalues[u]]);

////            dst[k++] = (src[ivalues[u]]>>2) & 0xFF;
////            x0 = (j*theta[0] + a1) * values[u] + p1;
////            x1 = (j*theta[3] + a2) * values[u] + p2;

//          // dst[k++] = interp2(src, op1[u], op2[u]);
//        }

////        memcpy (&dst[k], ivalues16, 4*sizeof (uint16_t));

////        uint16x4_t ires_16 = vld1_u16(ivalues16);
////        ires_16  = vshr_n_u16(ires_16, 2);

////        vst1_u16 ( &dst[k], ires_16);
////        k = k + 4;

////        uint16x4_t ires_16 = vqmovun_u32(ires);

////        float v1 = (a1/a4);
////        float v2 = (a2/a4);

////        a4 += theta[6];

////        x0 += (theta[0]/a4);
////        x1 += (theta[3]/a4);
//    }

//    memcpy (&dst[k], dst_tmp, FRAME_TAU_WIDTH*sizeof (uint8_t));

//}


void center_coords(CORNERS * cor)
{
    int i;
    cor->cm.x0 = 0;
    cor->cm.x1 = 0;

    // calculo del centroide
    for( i = 0; i < 4; i++ ){
        cor->cm.x0 += cor->c[i].x0;
        cor->cm.x1 += cor->c[i].x1;
    }
    cor->cm.x0 = roundf( cor->cm.x0 / 4.0f );
    cor->cm.x1 = roundf( cor->cm.x1 / 4.0f );

    // centrar coordenadas
    for( i = 0; i < 4; i++ ){
        cor->c[i].x0 -= cor->cm.x0;
        cor->c[i].x1 -= cor->cm.x1;
    }
}

int associate_coords(CORNERS * res, CORNERS *mov, uint8_t thr )
{
    uint8_t min_dists[4];
    uint8_t min_idx[4];
    uint8_t i;

    static CORNERS tmp;

    if( !res || !mov )
        return -1;

    // calcular distancias
    for( i = 0; i < 4; i++ ){
        min_L1_dist4(&res->c[i], mov, &min_dists[i], &min_idx[i]);

        // si alguna de las distancias es mayor al threshold
        // retornar sin modificar res
        if( min_dists[i] > thr){
            return 0;
        }
        tmp.c[i] = mov->c[min_idx[i]];
    }
    tmp.cm = mov->cm;
    *res = tmp;

    return 0;
}

void min_L1_dist4(COORDS * fix, CORNERS * mov, uint8_t *dist, uint8_t *idx)
{
    uint8_t d0, d1, d2, d3;
    d0 = abs(fix->x0 - mov->c[0].x0) + abs(fix->x1 - mov->c[0].x1);
    d1 = abs(fix->x0 - mov->c[1].x0) + abs(fix->x1 - mov->c[1].x1);
    d2 = abs(fix->x0 - mov->c[2].x0) + abs(fix->x1 - mov->c[2].x1);
    d3 = abs(fix->x0 - mov->c[3].x0) + abs(fix->x1 - mov->c[3].x1);

    if( dist && idx ){
        *dist = d0;
        *idx = 0;

        if( *dist > d1 ){
            *dist = d1;
            *idx = 1;
        }

        if( *dist > d2 ){
            *dist = d2;
            *idx = 2;
        }

        if( *dist > d3 ){
            *dist = d3;
            *idx = 3;
        }
    }else{
        // que le voy a hacer :/
    }
}

void update_roi_cm(COORDS *cm, int16_t * roi_x, int16_t *roi_y, int16_t roi_wh, int16_t max_h)
{
    *roi_x = 0.5*( *roi_x + cm->x0-roi_wh/2);
    *roi_y = 0.5*( *roi_y + cm->x1-roi_wh/2);

    if( *roi_x < 0 )
        *roi_x = 0;

    if( *roi_x + roi_wh > 640 )
        *roi_x = 639 - roi_wh;

    if( *roi_y < 0 )
        *roi_y = 0;

    if( *roi_y+roi_wh > max_h )
        *roi_y = max_h - roi_wh;
}

void updateROI(COORDS *cThm, QPoint *leftUp, QPoint *rightUp, QPoint *leftDown, QPoint *rightDown, bool forced)
{
    QPoint PosCentered;
    QPoint move;

   // for (int i = 0; i < 4; i++)
    {
        PosCentered.setX((rightUp->x()  + leftUp->x())/2);
        PosCentered.setY((leftDown->y() + leftUp->y())/2);

        move.setX(cThm->x0 - PosCentered.x());
        move.setY(cThm->x1 - PosCentered.y());
        //qDebug() << "Move" <<  move << "---" << (rightUp->x() - leftUp->x())/4 ;

        if( forced)
        {
            * leftUp    = * leftUp    + move;
            * leftDown  = * leftDown  + move;
            * rightUp   = * rightUp   + move;
            * rightDown = * rightDown + move;
        }
        else if (abs(move.x()) > (rightUp->x() - leftUp->x())/4 || abs(move.y()) > (leftDown->y() - leftUp->y())/4)
        {
            * leftUp    = * leftUp;
            * leftDown  = * leftDown;
            * rightUp   = * rightUp;
            * rightDown = * rightDown;
        }
    }
}
