#-------------------------------------------------
#
# Project created by QtCreator 2018-01-29T16:29:10
#
#-------------------------------------------------

QT       += core gui printsupport network

greaterThan(QT_MAJOR_VERSION, 4): QT +=  widgets
#INCLUDEPATH += /usr/include/opencv2/
TARGET = PLOTS_PRUEBA
TEMPLATE = app

QMAKE_CXXFLAGS_RELEASE -= -O
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE += -O2
QMAKE_CXXFLAGS += -fopenmp


LIBS += -L/home/thomas/Desktop/P_ELN/buildroot/build/host-gcc-final-7.3.0/build/arm-buildroot-linux-gnueabihf/libgomp -fopenmp
LIBS += -L/home/thomas/Desktop/P_ELN/buildroot/output/host/usr/lib \
\
 -lopencv_imgproc -lopencv_core -lm -lopencv_calib3d -lopencv_videoio -lopencv_imgcodecs

#LIBS += -L/home/thomas/Desktop/P_ELN/buildroot-2018.02.3/output/host/usr/lib \
 #-lopencv_imgproc -lopencv_core -lm -lopencv_calib3d -lopencv_videoio -lopencv_imgcodecs

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#target.path=/home/pi/Desktop
#target.path=/home/fa/Desktop
#INSTALLS += target

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    qcustomplot.cpp \
    labelclick.cpp \
    registration.cpp \
    segmentation.cpp \
    cs.cpp \
    tauudp.cpp \
    taunuc.cpp \
    classification.cpp \
    thermapp.cpp \
    labelclickir.cpp

HEADERS += \
    mainwindow.h \
    qcustomplot.h \
    labelclick.h \
    registration.h \
    segmentation.h \
    cs.h \
    tauudp.h \
    taunuc.h \
    classification.h \
    thermapp.h \
    labelclickir.h

FORMS += \
    mainwindow.ui \
    labelclick.ui \
    labelclickir.ui

LIBS += -lrt -lpthread -lusb-1.0 -lm
LIBS += -L/usr/local/lib -I/usr/include/opencv -lopencv_calib3d -lopencv_contrib -lopencv_core -lopencv_features2d -lopencv_flann -lopencv_gpu -lopencv_highgui -lopencv_imgproc -lopencv_legacy -lopencv_ml -lopencv_nonfree -lopencv_objdetect -lopencv_ocl -lopencv_photo -lopencv_stitching -lopencv_superres -lopencv_ts -lopencv_video -lopencv_videostab -L/lib64 -lGLU -lGL -ltbb -lrt -lpthread -lm -ldl
