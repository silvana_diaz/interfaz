#include "taunuc.h"
#include <QDebug>

TauNUC::TauNUC()
{
    for (int j = 1; j < 512; ++j)
    {
        for (int i = 0; i < 640; ++i)
        {
            int index = j*640+i;

            gain[index] = 2;
            offset[index] = 0;
        }
    }

}

void TauNUC::process()
{

}

void TauNUC::setParams( quint16 * gain, quint16 * offset )
{
    memcpy ( this->gain, gain, 640*512*2 );
    memcpy ( this->offset, offset, 640*512*2 );
}

void TauNUC::newFrame (quint16 * frame)
{
    memcpy ( FrameTau, frame, 640*512*2 );
    quint16 maximum = 0x0000;
    quint16 minimum = 0xFFFF;

    // Copia imagen
    for (int j = 1; j < 512; ++j)
    {
        for (int i = 0; i < 640; ++i)
        {
            int index = j*640+i;
            FrameTau[index] = gain[index]*FrameTau[index] + offset[index];
            quint16 value = FrameTau[index];

            FrameIMG[j*640+639-i]  = quint8(255*(value - last_min)/(last_max - last_min));

            if (value > maximum) maximum = value;
            if (value < minimum) minimum = value;
        }
    }

    last_min = minimum;
    last_max = maximum;

    emit processedFrame(FrameIMG);

}
