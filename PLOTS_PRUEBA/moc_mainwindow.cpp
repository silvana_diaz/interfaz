/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[54];
    char stringdata0[730];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 8), // "newFrame"
QT_MOC_LITERAL(2, 20, 0), // ""
QT_MOC_LITERAL(3, 21, 7), // "quint8*"
QT_MOC_LITERAL(4, 29, 5), // "frame"
QT_MOC_LITERAL(5, 35, 11), // "updateVideo"
QT_MOC_LITERAL(6, 47, 10), // "RGB_Harris"
QT_MOC_LITERAL(7, 58, 7), // "CORNERS"
QT_MOC_LITERAL(8, 66, 7), // "QImage&"
QT_MOC_LITERAL(9, 74, 3), // "img"
QT_MOC_LITERAL(10, 78, 15), // "IRHarrisOutputs"
QT_MOC_LITERAL(11, 94, 12), // "positionsROI"
QT_MOC_LITERAL(12, 107, 12), // "generateMask"
QT_MOC_LITERAL(13, 120, 4), // "cRGB"
QT_MOC_LITERAL(14, 125, 9), // "clickSegm"
QT_MOC_LITERAL(15, 135, 9), // "maskLists"
QT_MOC_LITERAL(16, 145, 4), // "posx"
QT_MOC_LITERAL(17, 150, 4), // "posy"
QT_MOC_LITERAL(18, 155, 8), // "getImage"
QT_MOC_LITERAL(19, 164, 5), // "image"
QT_MOC_LITERAL(20, 170, 15), // "uchar[480][640]"
QT_MOC_LITERAL(21, 186, 10), // "matRGBFull"
QT_MOC_LITERAL(22, 197, 16), // "realtimeDataSlot"
QT_MOC_LITERAL(23, 214, 27), // "on_action_Save_As_triggered"
QT_MOC_LITERAL(24, 242, 18), // "on_restart_clicked"
QT_MOC_LITERAL(25, 261, 17), // "on_replot_clicked"
QT_MOC_LITERAL(26, 279, 19), // "on_saveplot_clicked"
QT_MOC_LITERAL(27, 299, 15), // "on_open_clicked"
QT_MOC_LITERAL(28, 315, 13), // "update_window"
QT_MOC_LITERAL(29, 329, 16), // "on_close_clicked"
QT_MOC_LITERAL(30, 346, 15), // "on_snap_clicked"
QT_MOC_LITERAL(31, 362, 15), // "on_stop_clicked"
QT_MOC_LITERAL(32, 378, 18), // "on_addData_clicked"
QT_MOC_LITERAL(33, 397, 17), // "on_redraw_clicked"
QT_MOC_LITERAL(34, 415, 11), // "pinta_cuads"
QT_MOC_LITERAL(35, 427, 8), // "esquinas"
QT_MOC_LITERAL(36, 436, 15), // "on_quit_clicked"
QT_MOC_LITERAL(37, 452, 23), // "on_segmentation_clicked"
QT_MOC_LITERAL(38, 476, 17), // "get_otsuclick_pos"
QT_MOC_LITERAL(39, 494, 5), // "click"
QT_MOC_LITERAL(40, 500, 18), // "on_startIR_clicked"
QT_MOC_LITERAL(41, 519, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(42, 541, 23), // "on_pushButton_2_clicked"
QT_MOC_LITERAL(43, 565, 11), // "onSegmReady"
QT_MOC_LITERAL(44, 577, 8), // "getVideo"
QT_MOC_LITERAL(45, 586, 8), // "IRframes"
QT_MOC_LITERAL(46, 595, 25), // "on_segmentation_2_clicked"
QT_MOC_LITERAL(47, 621, 14), // "on_reg_clicked"
QT_MOC_LITERAL(48, 636, 5), // "Frame"
QT_MOC_LITERAL(49, 642, 6), // "short*"
QT_MOC_LITERAL(50, 649, 18), // "UpdateProcessFrame"
QT_MOC_LITERAL(51, 668, 26), // "on_gainSlider_valueChanged"
QT_MOC_LITERAL(52, 695, 5), // "value"
QT_MOC_LITERAL(53, 701, 28) // "on_offsetSlider_valueChanged"

    },
    "MainWindow\0newFrame\0\0quint8*\0frame\0"
    "updateVideo\0RGB_Harris\0CORNERS\0QImage&\0"
    "img\0IRHarrisOutputs\0positionsROI\0"
    "generateMask\0cRGB\0clickSegm\0maskLists\0"
    "posx\0posy\0getImage\0image\0uchar[480][640]\0"
    "matRGBFull\0realtimeDataSlot\0"
    "on_action_Save_As_triggered\0"
    "on_restart_clicked\0on_replot_clicked\0"
    "on_saveplot_clicked\0on_open_clicked\0"
    "update_window\0on_close_clicked\0"
    "on_snap_clicked\0on_stop_clicked\0"
    "on_addData_clicked\0on_redraw_clicked\0"
    "pinta_cuads\0esquinas\0on_quit_clicked\0"
    "on_segmentation_clicked\0get_otsuclick_pos\0"
    "click\0on_startIR_clicked\0on_pushButton_clicked\0"
    "on_pushButton_2_clicked\0onSegmReady\0"
    "getVideo\0IRframes\0on_segmentation_2_clicked\0"
    "on_reg_clicked\0Frame\0short*\0"
    "UpdateProcessFrame\0on_gainSlider_valueChanged\0"
    "value\0on_offsetSlider_valueChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      35,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  189,    2, 0x0a /* Public */,
       5,    0,  192,    2, 0x08 /* Private */,
       6,    2,  193,    2, 0x08 /* Private */,
      12,    2,  198,    2, 0x08 /* Private */,
      14,    2,  203,    2, 0x08 /* Private */,
      18,    2,  208,    2, 0x08 /* Private */,
      22,    0,  213,    2, 0x08 /* Private */,
      23,    0,  214,    2, 0x08 /* Private */,
      24,    0,  215,    2, 0x08 /* Private */,
      25,    0,  216,    2, 0x08 /* Private */,
      26,    0,  217,    2, 0x08 /* Private */,
      27,    0,  218,    2, 0x08 /* Private */,
      28,    0,  219,    2, 0x08 /* Private */,
      29,    0,  220,    2, 0x08 /* Private */,
      30,    0,  221,    2, 0x08 /* Private */,
      31,    0,  222,    2, 0x08 /* Private */,
      32,    0,  223,    2, 0x08 /* Private */,
      33,    0,  224,    2, 0x08 /* Private */,
      34,    1,  225,    2, 0x08 /* Private */,
      35,    0,  228,    2, 0x08 /* Private */,
      36,    0,  229,    2, 0x08 /* Private */,
      37,    0,  230,    2, 0x08 /* Private */,
      38,    1,  231,    2, 0x08 /* Private */,
      40,    0,  234,    2, 0x08 /* Private */,
      41,    0,  235,    2, 0x08 /* Private */,
      42,    0,  236,    2, 0x08 /* Private */,
      43,    0,  237,    2, 0x08 /* Private */,
      44,    0,  238,    2, 0x08 /* Private */,
      45,    0,  239,    2, 0x08 /* Private */,
      46,    0,  240,    2, 0x08 /* Private */,
      47,    0,  241,    2, 0x08 /* Private */,
      48,    1,  242,    2, 0x08 /* Private */,
      50,    0,  245,    2, 0x08 /* Private */,
      51,    1,  246,    2, 0x08 /* Private */,
      53,    1,  249,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    0x80000000 | 7, 0x80000000 | 8, 0x80000000 | 10,    9,   11,
    QMetaType::Void, QMetaType::QImage, 0x80000000 | 7,    9,   13,
    0x80000000 | 15, QMetaType::Int, QMetaType::Int,   16,   17,
    QMetaType::Void, 0x80000000 | 8, 0x80000000 | 20,   19,   21,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QPoint,   39,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::QPixmap, 0x80000000 | 49,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   52,
    QMetaType::Void, QMetaType::Int,   52,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->newFrame((*reinterpret_cast< quint8*(*)>(_a[1]))); break;
        case 1: _t->updateVideo(); break;
        case 2: { CORNERS _r = _t->RGB_Harris((*reinterpret_cast< QImage(*)>(_a[1])),(*reinterpret_cast< IRHarrisOutputs(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< CORNERS*>(_a[0]) = std::move(_r); }  break;
        case 3: _t->generateMask((*reinterpret_cast< QImage(*)>(_a[1])),(*reinterpret_cast< CORNERS(*)>(_a[2]))); break;
        case 4: { maskLists _r = _t->clickSegm((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< maskLists*>(_a[0]) = std::move(_r); }  break;
        case 5: _t->getImage((*reinterpret_cast< QImage(*)>(_a[1])),(*reinterpret_cast< uchar(*)[480][640]>(_a[2]))); break;
        case 6: _t->realtimeDataSlot(); break;
        case 7: _t->on_action_Save_As_triggered(); break;
        case 8: _t->on_restart_clicked(); break;
        case 9: _t->on_replot_clicked(); break;
        case 10: _t->on_saveplot_clicked(); break;
        case 11: _t->on_open_clicked(); break;
        case 12: _t->update_window(); break;
        case 13: _t->on_close_clicked(); break;
        case 14: _t->on_snap_clicked(); break;
        case 15: _t->on_stop_clicked(); break;
        case 16: _t->on_addData_clicked(); break;
        case 17: _t->on_redraw_clicked(); break;
        case 18: _t->pinta_cuads((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 19: _t->esquinas(); break;
        case 20: _t->on_quit_clicked(); break;
        case 21: _t->on_segmentation_clicked(); break;
        case 22: _t->get_otsuclick_pos((*reinterpret_cast< QPoint(*)>(_a[1]))); break;
        case 23: _t->on_startIR_clicked(); break;
        case 24: _t->on_pushButton_clicked(); break;
        case 25: _t->on_pushButton_2_clicked(); break;
        case 26: _t->onSegmReady(); break;
        case 27: _t->getVideo(); break;
        case 28: _t->IRframes(); break;
        case 29: _t->on_segmentation_2_clicked(); break;
        case 30: _t->on_reg_clicked(); break;
        case 31: { QPixmap _r = _t->Frame((*reinterpret_cast< short*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QPixmap*>(_a[0]) = std::move(_r); }  break;
        case 32: _t->UpdateProcessFrame(); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 35)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 35;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 35)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 35;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
