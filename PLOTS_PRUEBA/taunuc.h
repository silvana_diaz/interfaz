#ifndef TAUNUC_H
#define TAUNUC_H

#include <QObject>

class TauNUC : public QObject {
    Q_OBJECT
public:
    TauNUC();
public slots:
    void process();
    void newFrame (quint16 * frame);
    void setParams( quint16 * gain, quint16 * offset );

signals:

    void finished();
    void error(QString err);
    void processedFrame(quint8 *);

private:
    // add your variables here

    quint16 last_min, last_max;
    quint16 gain[512*640];
    quint16 offset[512*640];

    quint16 FrameTau[512*640];
    uchar FrameIMG[512*640];
};

#endif // TAUNUC_H
