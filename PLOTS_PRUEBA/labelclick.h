#ifndef LABELCLICK_H
#define LABELCLICK_H

#include <QWidget>
#include <QMouseEvent>
#include <QDebug>
#include <QPainter>

#include <QLabel>

namespace Ui {
class labelclick;
}

class labelclick : public QWidget
{
    Q_OBJECT

public:
    explicit labelclick(QWidget *parent = 0);
    ~labelclick();

    void setPixmap(QPixmap pix);

    const QPixmap &Pixmap();
    bool mousePressed;
    bool drawStarted;
    bool drawEnd;
    bool doneRect;
    bool cornersEnd;
    bool ir = false;
    int enter = 0 ;

    bool flag = false;

    bool otsuReady = false;
    bool segmReady = false;

    QPoint posRightUp[4];
    QPoint posRightDown[4];
    QPoint posLeftUp[4];
    QPoint posLeftDown[4];
    QLabel *label;
    float propw;
    float proph;
    QRect mRect;//estaba privado.

    QPoint beginR, endR;

    QPoint click;


private:
    Ui::labelclick *ui;

    QPainter painter;
    QPixmap mPix;


signals:
    void getCorners();
    void numcuad(int num);
    void pressed(QPoint click);
    void seeds(int x, int y);

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
    void getCornerData(int it);

signals:
private slots:
};


#endif // LABELCLICK_H
/*
#ifndef LABELCLICK_H
#define LABELCLICK_H

#include <QWidget>
#include <QMouseEvent>
#include <QDebug>
#include <QPainter>
#include <QLabel>

namespace Ui {
class labelclick;
}

class labelclick : public QWidget
{
    Q_OBJECT

public:
    explicit labelclick(QWidget *parent = 0);
    ~labelclick();

    void setPixmap(QPixmap pix);
    bool mousePressed;
    bool drawStarted;
    bool drawEnd;
    bool ir = false;
    bool doneRect;
    bool cornersEnd;
    int enter = 0 ;
    float propw;
    float proph;

    QPoint posRightUp[4];
    QPoint posRightDown[4];
    QPoint posLeftUp[4];
    QPoint posLeftDown[4];
    QLabel *label;
    QRect mRect;
private:
    Ui::labelclick *ui;

    QPainter painter;
    QPixmap mPix;


signals:
    void getCorners();
    void numcuad(int num);
protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);

    void getCornerData(int it);

signals:
private slots:
};

#endif // LABELCLICK_H
*/
