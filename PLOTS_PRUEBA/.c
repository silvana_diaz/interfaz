void MainWindow::ImageRegistration(uint16_t* uiIR, uint8_t* uiProy, vector<Point2f> corner_src, vector<Point2f> corner_dst)
{
    QElapsedTimer tim;
    tim.start();

    ui->textEdit->setText("Registering images");
    Mat fH = findHomography(corner_src, corner_dst); //Mat fH = findHomography(corner_dst, corner_src);

    theta[0] = double (float (fH.at<double>(0,0)));
    theta[1] = double (float (fH.at<double>(0,1)));
    theta[2] = double (float (fH.at<double>(0,2)));
    theta[3] = double (float (fH.at<double>(1,0)));
    theta[4] = double (float (fH.at<double>(1,1)));
    theta[5] = double (float (fH.at<double>(1,2)));
    theta[6] = double (float (fH.at<double>(2,0)));
    theta[7] = double (float (fH.at<double>(2,1)));
    theta[8] = double (float (fH.at<double>(2,2)));

//    warpPerspective(matIR, matOut, fH, matIR.size());
//    qDebug() << "Register time: " << tim.elapsed();

//    QImage imgIn= QImage((const unsigned char*)  matOut.data, matOut.cols, matOut.rows, matOut.step, QImage::Format_Grayscale8);
//    imgIn.save("/home/silvana/Documentos/thesis/imReg0.jpg");

   // int cont = 0;
   // while (!videoOver)
    {
        //center_coords(&corners_rgb);
        //c_assoc = corners_rgb;
        //center_coords(&corners_thm);

       // associate_coords(&c_assoc, &corners_thm, 25);
       // tform_estim(theta, eta,  &c_assoc, &corners_rgb);
       // qDebug() << "Theta" << theta[0] << "-" << theta[1];

//        for (int i = 0; i < FRAME_TAU_HEIGHT; i++)
//        {
//            apply_proy_tform_line(uiProy, uiIR, &corners_rgb.cm, &corners_thm.cm, theta, i);
//        }
    }

    if (firstframe)
    {
        avgTempLesion = apply_proy_tform_mask(uiIR, uiProy, true, theta, &lesionList);
        avgTempSkin = apply_proy_firstframe(uiIR, uiProy, false,theta, &skinList, &newMask, avgTempLesion);
       // qDebug() << avgTempLesion << avgTempSkin;
        firstframe = false;
    }
     avgTempLesion = apply_proy_tform_mask(uiIR, uiProy, true, theta, &lesionList);
     avgTempSkin = apply_proy_tform_mask(uiIR, uiProy, false, theta, &newMask);
//    pixelTempLesion = apply_proy_tform_mask(uiIR, uiProy, true, theta, &lesionList);
//    pixelTempSkin = apply_proy_tform_mask(uiIR, uiProy, false, theta, &skinList);

//    allSkinValues.append(pixelTempSkin);
//    allLesionValues.append(pixelTempLesion);
     skin.append(avgTempSkin);
     lesion.append(avgTempLesion);

     qDebug() << "Size: " <<lesion.size();
   //qDebug() << "Register time: " << tim.elapsed();

    QImage imgReg = QImage((uint8_t*) uiProy, FRAME_TAU_WIDTH, FRAME_TAU_HEIGHT, FRAME_TAU_WIDTH, QImage::Format_Grayscale8);
    ui->posClick->setPixmap(QPixmap::fromImage(imgReg));
    //imgReg.save("/home/silvana/Documentos/thesis/imReg.png");
}
