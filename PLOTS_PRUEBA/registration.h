#include <stdint.h>
#include <QPoint>
#include <QList>
#ifndef REGISTRATION_H
#define REGISTRATION_H

//512U
//640U
#define FRAME_TAU_HEIGHT 		384U
#define FRAME_TAU_WIDTH 		288U

typedef struct{
    int x0;
    int x1;
}COORDS;

typedef struct{
    COORDS c[4]; // coordenadas de las esquinas (centradas o no)
    COORDS cm;   // coordenadas del centroide
}CORNERS;

typedef struct{
    float x0;
    float x1;
}COORDS_f;

typedef struct{
    COORDS_f c[4]; // coordenadas de las esquinas (centradas o no)
    COORDS_f cm;   // coordenadas del centroide
}CORNERS_f;


typedef struct {
    QList<QPoint> lesionList;
    QList<QPoint> skinList;
}maskLists;

typedef struct  {
    float tempLesion;
    float tempSurround;
}temps;

typedef struct  {
    CORNERS cThm;
    temps t;
}IRvideoOutputs;

typedef struct  {
    CORNERS cThm;
    QPoint * UR;
    QPoint * LR;
    QPoint * UL;
    QPoint * LL;
}IRHarrisOutputs;


float apply_proy_firstframe (uint16_t *src, uint8_t *dst, bool lesion, float theta[8], QList<QPoint> * mask, QList<QPoint> * maskOut, float avgTempLesionFirstFrame);
float apply_proy_tform_mask (uint16_t *src, uint8_t *dst, bool lesion, float theta[8], QList<QPoint> * mask, CORNERS *corners_proy, CORNERS c_assoc);
void apply_proy_tform_line(uint8_t * src, uint8_t *dst, COORDS * cm_src, \
        COORDS * cm_dst, float theta[6], int i );
int tform_error(float theta[8], CORNERS *fix, CORNERS *mov, CORNERS_f *err);
int tform_corners(float theta[8], CORNERS *c, CORNERS_f *dest);
void center_coords(CORNERS * cor);
void tform_estim(float theta[8], float eta[2], CORNERS * fix, CORNERS * mov);
int associate_coords(CORNERS * res, CORNERS *mov, uint8_t thr );
void min_L1_dist4(COORDS * fix, CORNERS * mov, uint8_t *dist, uint8_t *idx);
void apply_proy_Neon(uint8_t * src, uint8_t *dst, COORDS * cm_src, COORDS * cm_dst, float theta[8], int i);
void update_roi_cm(COORDS *cm, int16_t * roi_x, int16_t *roi_y, int16_t roi_wh, int16_t max_h);
void updateROI(COORDS *cThm, QPoint *leftUp, QPoint *rightUp, QPoint *leftDown, QPoint *rightDown, bool forced);

#endif // REGISTRATION_H
