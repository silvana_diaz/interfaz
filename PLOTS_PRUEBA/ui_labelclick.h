/********************************************************************************
** Form generated from reading UI file 'labelclick.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LABELCLICK_H
#define UI_LABELCLICK_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_labelclick
{
public:
    QHBoxLayout *horizontalLayout;
    QLabel *label;

    void setupUi(QWidget *labelclick)
    {
        if (labelclick->objectName().isEmpty())
            labelclick->setObjectName(QStringLiteral("labelclick"));
        labelclick->resize(400, 300);
        horizontalLayout = new QHBoxLayout(labelclick);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(labelclick);
        label->setObjectName(QStringLiteral("label"));
        label->setMaximumSize(QSize(640, 480));
        label->setFrameShape(QFrame::Box);
        label->setScaledContents(true);
        label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        horizontalLayout->addWidget(label);


        retranslateUi(labelclick);

        QMetaObject::connectSlotsByName(labelclick);
    } // setupUi

    void retranslateUi(QWidget *labelclick)
    {
        labelclick->setWindowTitle(QApplication::translate("labelclick", "Form", nullptr));
        label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class labelclick: public Ui_labelclick {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LABELCLICK_H
