#ifndef TAUUDP_H
#define TAUUDP_H

#include <QObject>

class TauUDP : public QObject {
    Q_OBJECT
public:
    TauUDP();
public slots:
    void process();

signals:
    void newFrame ( quint16 * ptr );
    void finished();
    void error(QString err);
private:
    // add your variables here
    quint16 FrameTau[512*640];

};

#endif // TAUUDP_H
