#include "segmentation.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <segmentation.h>
#include <qdebug.h>
#include <qimage.h>
#include <qfile.h>
#include <cs.h>

#define FRAME_TAU_WIDTH 		640U

uint8_t mat[ROWROI][COLROI][3];

//#define EPSILON     1e-5
//#define VARSIZE     ((ROWROI*COLROI*2)-(ROWROI+COLROI))
//#define VAR2SIZE    (ROWROI*COLROI) + 2*VARSIZE
//#define CONST (ROWROI*COLROI-2)
////RW
//sparse F1[(ROWROI*COLROI) + 2*((ROWROI*COLROI*2)-(ROWROI+COLROI))]; // 2 vectores de posiciones + 1 de valores en matriz laplaciana
//sparse x1[(ROWROI*COLROI) + 2*((ROWROI*COLROI*2)-(ROWROI+COLROI))] = {};

//int edges1[ROWROI*COLROI*2][2];
//int edges2[(ROWROI*COLROI*2)-(ROWROI+COLROI)][2];
//sparse i1[2*VARSIZE];
//double diag[2*(ROWROI*COLROI*2)-(ROWROI+COLROI)];
//double jvar1[2*(ROWROI*COLROI*2)-(ROWROI+COLROI)];

//int antiIndex[ROWROI*COLROI];
//int antiIndex2[CONST];



bool readyOtsu = false;
bool readyRW = false;
bool otsu_ready = false;


void multithresholding(uint8_t *mat_otsu, uint8_t * mat, int row, int col, bool binary, int binaryChoice)
{
    int k=0;
    int j = 0;
    int i = 0;

    QTime s;
    s.start();
    int I[row*col][3];
    double xc[row*col][3];


    while (k < row*col)
    {
        I[k][0] = * (mat + j*3 + i*3*col);
        I[k][1] = * (mat + j*3 + i*3*col + 1);
        I[k][2] = * (mat + j*3 + i*3*col + 2);

        k++;
        i++;

        if (i >= row)
        {
            i = 0;
            j++;
        }

//        if (i < row)
//        {
//            I[k][0] = mat[i][j][0]; //img_R
//            I[k][1] = mat[i][j][1]; //img_G
//            I[k][2] = mat[i][j][2]; //img_B
//            i = i+1;
//            k = k+1;
//        }
//        else if (i == row)
//        {
//            I[k][0] = mat[0][j+1][0];
//            I[k][1] = mat[0][j+1][1];
//            I[k][2] = mat[0][j+1][2];
//            i = i+1;
//            k = k+1;
//        }
//        else
//        {
//            i = 1;
//            j = j+1;
//        }
    }

    qDebug() << "hola Otsu";

    int s1 = 0;
    int s2 = 0;
    int s3 = 0;


    for (int i = 0; i < row*col; i++)
    {
        s1 += I[i][0];
        s2 += I[i][1];
        s3 += I[i][2];
    }

    float sum1 = s1/float(row*col);
    float sum2 = s2/float(row*col);
    float sum3 = s3/float(row*col);

    for (int i = 0; i < row*col; i++)
    {
        xc[i][0] = I[i][0] - sum1;
        xc[i][1] = I[i][1] - sum2;
        xc[i][2] = I[i][2] - sum3;

    }

    float xCOL[3][3]; // xc' * xc;

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            xCOL[i][j] = 0;
            for (int k = 0; k < row*col; k++)
            {
                xCOL[i][j] += (xc[k][i] * xc[k][j]);

            }
        }
    }

    float D[3]; //diag(xCOL)

    for (int i=0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            if (i == j)
            {
                D[i] = xCOL[i][j];
            }
        }
    }

    int b_max;

    //max(D)
    if ((D[0] > D[1]) && (D[0]> D[2]))
    {
        b_max = 1;
    }
    else if ((D[1] > D[0]) && (D[1]> D[2]))
    {
        b_max = 2;
    }
    else
    {
        b_max = 3;
    }
    // cv::Mat& src - source image's matrix
        int histogram[256] = { 0 };
        int pixelsCount = row*col;

        for (int y = 0; y < row; y++)
        {
            for (int x = 0; x < col; x++)
            {
                for (int z = 0; z < 2; z++)
                {
                    uchar value = *(mat + z + x*3 + y*col*3); // mat[y][x][z]
                    histogram[value]++;
                }
            }
        }

        double c = 0;
        double Mt = 0;

        double p[256] = { 0 };
        for (int i = 0; i < 256; i++)
        {
            p[i] = (double) histogram[i] / (double) pixelsCount;
            Mt += i * p[i];
        }

        int optimalTreshold1 = 0;
        int optimalTreshold2 = 0;
        int optimalTreshold3 = 0;

        double maxBetweenVar = 1;

        double w0 = 0;
        double m0 = 0;
        double c0 = 0;
        double p0 = 0;

        double w1 = 0;
        double m1 = 0;
        double c1 = 0;
        double p1 = 0;

        double w2 = 0;
        double m2 = 0;
        double c2 = 0;
        double p2 = 0;
        for (int tr1 = 0; tr1 < 256; tr1++)
        {
            p0 += p[tr1];
            w0 += (tr1 * p[tr1]);
            if (p0 != 0)
            {
                m0 = w0 / p0;
            }

            c0 = p0 * (m0 - Mt) * (m0 - Mt);

            c1 = 0;
            w1 = 0;
            m1 = 0;
            p1 = 0;
            for (int tr2 = tr1 + 1; tr2 < 256; tr2++)
            {

                p1 += p[tr2];
                w1 += (tr2 * p[tr2]);
                if (p1 != 0)
                {
                    m1 = w1 / p1;
                }

                c1 = p1 * (m1 - Mt) * (m1 - Mt);


                c2 = 0;
                w2 = 0;
                m2 = 0;
                p2 = 0;
                for (int tr3 = tr2 + 1; tr3 < 256; tr3++)
                {

                    p2 += p[tr3];
                    w2 += (tr3 * p[tr3]);
                    if (p2 != 0)
                    {
                        m2 = w2 / p2;
                    }

                    c2 = p2 * (m2 - Mt) * (m2 - Mt);

                    double p3 = 1 - (p0 + p1 + p2);
                    double w3 = Mt - (w0 + w1 + w2);
                    double m3 = w3 / p3;
                    double c3 = p3 * (m3 - Mt) * (m3 - Mt);

                    double c = c0 + c1 + c2 + c3;

                    if (maxBetweenVar < c)
                    {
                        maxBetweenVar = c;
                        optimalTreshold1 = tr1;
                        optimalTreshold2 = tr2;
                        optimalTreshold3 = tr3;
                    }
                }
            }
        }
    qDebug()<< "Umbrales :" << optimalTreshold1<<optimalTreshold2<<optimalTreshold3;

    int sum1_opt = 0;
    int sum2_opt = 0;
    int sum3_opt = 0;
    int sum4_opt = 0;

    int index1 = 0;
    int index2 = 0;
    int index3 = 0;
    int index4 = 0;

    int matVar = 0;

    for (int i=0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            matVar = *(mat + b_max + j*3 + i*col*3);
            if (matVar <= optimalTreshold1)
            {
                sum1_opt = sum1_opt + matVar;
                index1 = index1 + 1;
            }
            if (matVar > optimalTreshold1 && matVar <= optimalTreshold2)
            {
                sum2_opt = sum2_opt +matVar;
                index2=index2+1;

            }
            else if (matVar > optimalTreshold2 && matVar <= optimalTreshold3)
            {
                sum3_opt = sum3_opt +matVar;
                index3=index3+1;
            }
            else
            {
                sum4_opt = sum4_opt + matVar;
                index4 = index4 + 1;
            }
        }
    }

//    qDebug() << sum1_opt/index1;
//    qDebug() << sum2_opt/index2;
//    qDebug() << sum3_opt/index3;
//    qDebug() << sum4_opt/index4;

    if (binary)
    {
        //qDebug() << "wooop";
        int optThresh;
        if (binaryChoice == 0)
            optThresh = optimalTreshold1;
        else if (binaryChoice == 1)
            optThresh = optimalTreshold2;
        else if (binaryChoice == 2)
            optThresh = optimalTreshold3;

        for (int i=0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                matVar = *(mat + b_max + j*3 + i*col*3);
                uint8_t * matOtsu = (mat_otsu + j + i*col);

                if (matVar < optThresh)
                {
                    * matOtsu = 0; //sum1_opt/index1;
                }
                else
                    * matOtsu = 255; //sum4_opt/index4;
            }

        }
        if (optThresh == 3)
        {
            for (int i=0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    matVar = *(mat + b_max + j*3 + i*col*3);
                    uint8_t * matOtsu = (mat_otsu + j + i*col);

                 //   qDebug() << "otsuuu";
                    if (matVar > optThresh)
                        * matOtsu = 255; //sum4_opt/index4;
                }
            }
        }
    }
    else
    {
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                matVar = *(mat + b_max + j*3 + i*col*3);
                uint8_t * matOtsu = (mat_otsu + j + i*col);

                if (matVar < optimalTreshold1)
                {
                    * matOtsu = 0; //sum1_opt/index1;
                }
                else if (matVar >= optimalTreshold1 && (matVar < optimalTreshold2))
                    * matOtsu = 64;// sum2_opt/index2;
                else if (matVar >= optimalTreshold2 && (matVar < optimalTreshold3))
                   * matOtsu =  128;//sum3_opt/index3;
                else
                    * matOtsu = 255; //sum4_opt/index4;

//                /*if (mat[i][j][b_max] < optimalTreshold1)
//                    mat_otsu[i][j] = 1;
//                else if (mat[i][j][b_max] >= optimalTreshold1 && (mat[i][j][b_max] < optimalTreshold2))
//                    mat_otsu[i][j] = 2;
//                else if (mat[i][j][b_max] >= optimalTreshold2 && (mat[i][j][b_max] < optimalTreshold3))
//                    mat_otsu[i][j] = 3;
//                else
//                    mat_otsu[i][j] = 4;*/
            }
        }
    }
}

void binarization(uint8_t * mat_otsu, uint8_t * mat, int row, int col, int optThresh)
{
    int matVar=0;

    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            matVar = *(mat + j + i*col);
            uint8_t * matOtsu = (mat_otsu + j + i*col);

            if (matVar <= optThresh)
            {
                * matOtsu = 0;
            }
            else
                * matOtsu = 255;
        }
    }
}
void greyMultithresholding(uint8_t *mat_otsu, uint8_t * mat, int row, int col)
{
    int k=0;
    int j = 0;
    int i = 0;

    QTime s;
    s.start();


    qDebug() << "hola Otsu";


    // cv::Mat& src - source image's matrix
        int histogram[256] = { 0 };
        int pixelsCount = row*col;

        for (int y = 0; y < row; y++)
        {
            for (int x = 0; x < col; x++)
            {
                uchar value = *(mat + x + y*col); // mat[y][x][z]
                histogram[value]++;

            }
        }

        double c = 0;
        double Mt = 0;

        double p[256] = { 0 };
        for (int i = 0; i < 256; i++)
        {
            p[i] = (double) histogram[i] / (double) pixelsCount;
            Mt += i * p[i];
        }

        int optimalTreshold1 = 0;
        int optimalTreshold2 = 0;
        int optimalTreshold3 = 0;

        double maxBetweenVar = 1;

        double w0 = 0;
        double m0 = 0;
        double c0 = 0;
        double p0 = 0;

        double w1 = 0;
        double m1 = 0;
        double c1 = 0;
        double p1 = 0;

        double w2 = 0;
        double m2 = 0;
        double c2 = 0;
        double p2 = 0;
        for (int tr1 = 0; tr1 < 256; tr1++)
        {
            p0 += p[tr1];
            w0 += (tr1 * p[tr1]);
            if (p0 != 0)
            {
                m0 = w0 / p0;
            }

            c0 = p0 * (m0 - Mt) * (m0 - Mt);

            c1 = 0;
            w1 = 0;
            m1 = 0;
            p1 = 0;
            for (int tr2 = tr1 + 1; tr2 < 256; tr2++)
            {

                p1 += p[tr2];
                w1 += (tr2 * p[tr2]);
                if (p1 != 0)
                {
                    m1 = w1 / p1;
                }

                c1 = p1 * (m1 - Mt) * (m1 - Mt);


                c2 = 0;
                w2 = 0;
                m2 = 0;
                p2 = 0;
                for (int tr3 = tr2 + 1; tr3 < 256; tr3++)
                {

                    p2 += p[tr3];
                    w2 += (tr3 * p[tr3]);
                    if (p2 != 0)
                    {
                        m2 = w2 / p2;
                    }

                    c2 = p2 * (m2 - Mt) * (m2 - Mt);

                    double p3 = 1 - (p0 + p1 + p2);
                    double w3 = Mt - (w0 + w1 + w2);
                    double m3 = w3 / p3;
                    double c3 = p3 * (m3 - Mt) * (m3 - Mt);

                    double c = c0 + c1 + c2 + c3;

                    if (maxBetweenVar < c)
                    {
                        maxBetweenVar = c;
                        optimalTreshold1 = tr1;
                        optimalTreshold2 = tr2;
                        optimalTreshold3 = tr3;
                    }
                }
            }
        }
    qDebug()<< "Umbrales :" << optimalTreshold1<<optimalTreshold2<<optimalTreshold3;

    int sum1_opt = 0;
    int sum2_opt = 0;
    int sum3_opt = 0;
    int sum4_opt = 0;

    int index1 = 0;
    int index2 = 0;
    int index3 = 0;
    int index4 = 0;

    int matVar = 0;

    for (int i=0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            matVar = *(mat + j + i*col);
            if (matVar <= optimalTreshold1)
            {
                sum1_opt = sum1_opt + matVar;
                index1 = index1 + 1;
            }
//            if (matVar > optimalTreshold1 && matVar <= optimalTreshold2)
//            {
//                sum2_opt = sum2_opt +matVar;
//                index2=index2+1;

//            }
//            else if (matVar > optimalTreshold2 && matVar <= optimalTreshold3)
//            {
//                sum3_opt = sum3_opt +matVar;
//                index3=index3+1;
//            }
            else
            {
                sum4_opt = sum4_opt + matVar;
                index4 = index4 + 1;
            }
        }
    }

    qDebug() << sum1_opt/index1;
//    qDebug() << sum2_opt/index2;
//    qDebug() << sum3_opt/index3;
    qDebug() << sum4_opt/index4;
    for (int i=0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            matVar = *(mat + j + i*col);
            uint8_t * matOtsu = (mat_otsu + j + i*col);

            if (matVar < optimalTreshold1)
            {
                * matOtsu = 0; //sum1_opt/index1;
            }
//            else if (matVar >= optimalTreshold1 && (matVar < optimalTreshold2))
//                * matOtsu = sum2_opt/index2;
//            else if (matVar >= optimalTreshold2 && (matVar < optimalTreshold3))
//               * matOtsu = sum3_opt/index3;
            else
                * matOtsu = 255; //sum4_opt/index4;

            /*if (mat[i][j][b_max] < optimalTreshold1)
                mat_otsu[i][j] = 1;
            else if (mat[i][j][b_max] >= optimalTreshold1 && (mat[i][j][b_max] < optimalTreshold2))
                mat_otsu[i][j] = 2;
            else if (mat[i][j][b_max] >= optimalTreshold2 && (mat[i][j][b_max] < optimalTreshold3))
                mat_otsu[i][j] = 3;
            else
                mat_otsu[i][j] = 4;*/
        }
    }

}


void RW(uint8_t * otsu_img, uint8_t * mask_final, int seeds[2], int row, int col)
{
    qDebug() << "holo Walker";

    int i, j, h, k;
    float EPSILON    = 1e-5;
    int VARSIZE = ((row*col*2)-(row+col));
    int VAR2SIZE   = (row*col) + 2*VARSIZE;
    int CONST  = (row*col-2);

    //RW
    sparse F1[(row*col) + 2*((row*col*2)-(row*col))]; // 2 vectores de posiciones + 1 de valores en matriz laplaciana
    sparse x1[(row*col) + 2*((row*col*2)-(row*col))] = {};

    int edges1[row*col*2][2];
    int edges2[(row*col*2)-(row+col)][2];
    sparse i1[2*VARSIZE];
    double diag[2*(row*col*2)-(row+col)];
    double jvar1[2*(row*col*2)-(row+col)];

    int antiIndex[row*col];
    int antiIndex2[CONST];

    double B1[CONST]  = {};
    double B0[CONST]  = {};


    //Generar retículo cartesiano 2D

    //Conectar puntos
    for (i = 0; i < row*col; ++i)
    {
        edges1[i][0] = i;
        edges1[i][1] = i + 1;
    }

    for (i = row*col; i < row*col*2; ++i)
    {
        edges1[i][0] = edges1[i-row*col][0];
        edges1[i][1] = edges1[i-row*col][0] + row;
    }

    int out[row+1];

    for (i = 0, j = 0; j < row*col*2; j++)
    {
        if ( (edges1[j][0]>=row*col)||(edges1[j][0]<0)||(edges1[j][1]>=row*col)||(edges1[j][1]<0) )
        {
            out[i] = j;
            i = i + 1;
        }
    }

    // sort
    int t1[row+col];
    int D[col-1];
    int ans = row-1;

    for (j = 0; j< col-1; j++)
    {
        D[j] = ans;
        ans = ans + (row);
    }

    for (i = 0; i < row+col; ++i)
    {
        t1[i] = out[i];
        if (i > row)
        {
            for (j = 0; j < col-1; ++j)
            {
                t1[j+row+1] = D[j];
            }
        }
    }

    for (i = 0; i < row+col; ++i)
    {
        for (j = i+1; j < row+col; ++j)
        {
            if (t1[i] > t1[j])
            {
                int a = t1[i];
                t1[i] = t1[j];
                t1[j] = a;
            }
        }
    }

    i = 0;
    j = 0;
    k = 0;

    while (k < VARSIZE)
    {
        while (i < (row*col*2))
        {
            edges2[k][0] = edges1[i][0];
            edges2[k][1] = edges1[i][1];
            i = i+1;
            k = k+1;

            if (i == t1[j] && i < (row*col*2))
            {
                edges2[k][0] = edges1[i+1][0];
                edges2[k][1] = edges1[i+1][1];
                i = i+2;
                k = k+1;
                j = j+1;
            }
        }
    }

    //imgVals = img(:); This part only works for square images so far -.-
    //imgVals = Ip
//    for(k = 0; k < row*col; ++k)
//    {
//        int i = k / row;
//        int j = k % row;

//        Ip[k] = otsu_img[i][j];
//    }

    uint8_t * Ip = (uint8_t *) otsu_img;
float weights;
    //Gaussian weights
    for (i = 0; i < VARSIZE; ++i)
    {
        int valDistances    = sqrt(((Ip[edges2[i][0]] - Ip[edges2[i][1]])*(Ip[edges2[i][0]] - Ip[edges2[i][1]])));
        weights       = exp( -(90 * valDistances) ) + EPSILON;

        i1[i].i             = edges2[i][0];
        i1[i+VARSIZE].i     = edges2[i][1];
        i1[i].j             = edges2[i][1];
        i1[i+VARSIZE].j     = edges2[i][0];
        i1[i].value         = weights;
        i1[i+VARSIZE].value = weights;
    }
//qDebug() << weights;

    for (i = 0; i < 2*VARSIZE; ++i)
    {
        diag[i1[i].i]  += i1[i].value;
        jvar1[i]        = i;

        F1[i].i     = i1[i].i;
        F1[i].j     = i1[i].j;
        F1[i].value = i1[i].value;
//        if(F1[i].i == F1[i].j +1)
//        qDebug() << F1[i].value;
    }

    for (i = 2*VARSIZE; i < VAR2SIZE; ++i)
    {
        F1[i].i     = jvar1[i - 2*VARSIZE];
        F1[i].j     = jvar1[i - 2*VARSIZE];
        F1[i].value = diag[i - 2*VARSIZE];
    }

    //Hasta aqui F1 = L
    //Dirichlet problem, solve for RW probabilities
    int boundary[2][2] = { {1,0}, {0,1} };

    for (i = 0; i < row*col; ++i)
    {
        antiIndex[i] = i;
    }

    qDebug() << "holo";

    i = 0;
    j = 0;
    k = 0;

    while (i < row*col)
    {
        while (j < CONST)
        {
            antiIndex2[j] = antiIndex[i];
            i = i+1;
            j = j+1;

            if (i == seeds[k])
            {
                antiIndex2[j] = antiIndex[i+1];
                i = i+2;
                j = j+1;
                k = 1;
            }

        }
    }

    int x_idx = 0;

    for (i = 0; i < VAR2SIZE; i++)
    {
        if (F1[i].j == seeds[0])
        {
            for (j = 0; j < CONST; ++j)
            {
                if (F1[i].i == antiIndex2[j])
                {
                    B0[j] = F1[i].value;
                   qDebug() <<F1[i].i<< "B0" << B0[j] << "F1" << F1[i].value;
                }
           }
        }

        else if (F1[i].j == seeds[1])
        {
            for (j = 0; j < CONST; ++j)
            {
                if (F1[i].i == antiIndex2[j])
                {
                    B1[j] = F1[i].value;
                }
            }
        }

        if (F1[i].i != F1[i].j)
        {
            F1[i].value = - F1[i].value;
        }


        if (F1[i].i == seeds[0] || F1[i].i == seeds[1] || F1[i].j == seeds[0] || F1[i].j == seeds[1] )
        {
            x1[i].value = 0;
        }
        else
        {
            x1[i].value = F1[i].value;
        }
    }



    QTime d;
    d.start();


    for (int i = 0; i < VAR2SIZE; ++i)
    {
        for (int j=0; j < 2; ++j)
        {
            int *F_idx   = ( ((int *) &F1[i]) + j);
            int *x1_idx  = ( ((int *) &x1[i]) + j);

            if (*F_idx == seeds[0] || *F_idx == seeds[1])
            {
                *x1_idx = 0;
            }

            else if (*F_idx > seeds[1])   *x1_idx = *F_idx - 2;
            else if (*F_idx > seeds[0])   *x1_idx = *F_idx - 1;
            else                          *x1_idx = *F_idx;
        }
    }


    qDebug() << "Time Init" << d.elapsed();

    d.restart();

    // Sparse matriz

    cs *T = cs_spalloc(0, 0, CONST, 1, 1);
    for (int i = 0; i < VAR2SIZE; i++)
    {
        if (x1[i].value != 0)
        {
            cs_entry (T, x1[i].i, x1[i].j, x1[i].value);
        }
    }

    cs * C = cs_compress (T);
    QTime chol;
    chol.start();
    cs_cholsol(1, C, B0);
//    cs_lusol(1, C, B0, 1e-5);


    qDebug() << "cholesky" << chol.elapsed();

   // Generate mask
    for (h = 0; h < 2; ++h)
    {
        int index = seeds[h];

        i = index / row;
        j = index % row;

        //mask_final[j][i]
       *(mask_final + i + j*row)= (boundary[h][0] > boundary[h][1]) ? 0 : 255 ;
    }

    for (h = 0; h < CONST; ++h)
    {
        int index = antiIndex2[h];

        i = index / row;
        j = index % row;

       //mask_final[i][j] = (B0[h] > B1[h]) ? 0 : 255;
        //mask_final[i][j]
        *(mask_final + j + i*col) = (B0[index] > 0.5) ? 0 : 255;
    }
}

void erode(uint8_t * mask, int miniCol, int miniRow, uint8_t * mask_erode, int SE)
{
    int sr,er,sc,ec;
    uchar scratch;
    int k = 0;

    int m2 = round(SE/2.0);
    int n2 = round(SE/2.0);
    qDebug() << miniRow << miniCol;

    for (int r = 0; r < miniRow; r++)
    {
        for (int c = 0; c < miniCol; c++)
        {
            sr = maxi(r-m2,1);
            er = mini(r+m2,miniRow);
            sc = maxi(c-n2,1);
            ec = mini(c+n2,miniCol);
            scratch = 0;

            for (int i = sr; i < er; i++)
            {
                for (int j = sc; j < ec; j++)
                {
                    if (*(mask + i * miniCol + j) == 0 )
                    {
                        scratch = 255;
                        //(* size_mask )++;
                        break;
                    }
                }
            }

            *(mask_erode + r*miniCol + c) = scratch;
            //mask_dilate[r][c] = scratch;
//            if (mask_erode[r][c] == 255 )
//            {
//                list_of_values_mask[0][k] = r; //(r+min_corner_y)*FRAME_TAU_WIDTH + c+min_corner_x;
//                list_of_values_mask[1][k] = c;
//                k++;
//            }
        }
    }
//     * size_mask = k;
//    qDebug() << k ;

}

void dilate(uint8_t * mask, int miniCol, int miniRow, uint8_t * mask_dilate, int SE)
{
    QTime s;
    s.start();
    int sr,er,sc,ec;
    uchar scratch;
    int k = 0;

   // * size_mask = 0;


    int m2 = round(SE/2.0);
    int n2 = round(SE/2.0);

    for (int r = 0; r < miniRow; r++)
    {
        for (int c = 0; c < miniCol; c++)
        {
            sr = maxi(r-m2,1);
            er = mini(r+m2,miniRow);
            sc = maxi(c-n2,1);
            ec = mini(c+n2,miniCol);
            scratch = 0;

            for (int i = sr; i < er; i++)
            {
                for (int j = sc; j < ec; j++)
                {
                    if (*(mask + i * miniCol + j) == 0 )
                    {
                        scratch = 255;
                        //(* size_mask )++;
                        break;
                    }
                }
            }
            //mask_erode[r][c] = scratch;
            *(mask_dilate + r * miniCol + c) = scratch;
//            if (mask_dilate[r][c] == 255 )
//            {
//                list_of_values_mask[0][k] = r; //(r+min_corner_y)*FRAME_TAU_WIDTH + c+min_corner_x;
//                list_of_values_mask[1][k] = c;
//                k++;
//            }
        }
    }
//     * size_mask = k;
//    qDebug() << k ;

}


//int minCorners(int a, int b, int c, int d)
//{
//    if(a <= b && a <= c && a <= d) return a;
//    else if(b <= a && b <= c && b <= d) return b;
//    else if(c <= a && c <= b && c <= d) return c;
//    else return d;
//}

//    }

////    if (option == 1)
////    {
////        int m2 = round(SE2/2.0);
////        int n2 = round(SE2/2.0);

////        for (int r = 0; r < ROWROI; r++)
////        {
////            for (int c = 0; c < COLROI; c++)
////            {
////                sr = maxi(r-m2,1);
////                er = mini(r+m2,ROWROI);
////                sc = maxi(c-n2,1);
////                ec = mini(c+n2,COLROI);
////                scratch = 255;

////                for (int i = sr; i < er; i++)
////                {
////                    for (int j = sc; j < ec; j++)
////                    {
////                        if (mask[i][j] < 255)
////                        {
////                            scratch = 0;
////                           (* size_mask )++;
////                            break;
////                        }
////                    }
////                }
////                mask_erode_dilate[r][c] = scratch;
////                if (mask_erode_dilate[r][c] == 255)
////                {
////                    list_of_values_background[k][0] = r;
////                    list_of_values_background[k][1] = c;
////                    k++;
////                }
////            }
////        }
////    }
//}

int maxCorners(int a, int b, int c, int d)
{
    if(a >= b && a >= c && a >= d) return a;
    else if(b >= a && b >= c && b >= d) return b;
    else if(c >= a && c >= b && c >= d) return c;
    else return d;
}

int minCorners(int a, int b, int c, int d)
{
    if(a <= b && a <= c && a <= d) return a;
    else if(b <= a && b <= c && b <= d) return b;
    else if(c <= a && c <= b && c <= d) return c;
    else return d;
}
bool convolve2D(unsigned char* in, unsigned char* out, int dataSizeX, int dataSizeY,
                float* kernel, int kernelSizeX, int kernelSizeY)
{
    int i, j, m, n;
    unsigned char *inPtr, *inPtr2, *outPtr;
    float *kPtr;
    int kCenterX, kCenterY;
    int rowMin, rowMax;                             // to check boundary of input array
    int colMin, colMax;                             //
    float sum;                                      // temp accumulation buffer

    // check validity of params
    if(!in || !out || !kernel) return false;
    if(dataSizeX <= 0 || kernelSizeX <= 0) return false;

    // find center position of kernel (half of kernel size)
    kCenterX = kernelSizeX >> 1;
    kCenterY = kernelSizeY >> 1;

    // init working  pointers
    inPtr = inPtr2 = &in[dataSizeX * kCenterY + kCenterX];  // note that  it is shifted (kCenterX, kCenterY),
    outPtr = out;
    kPtr = kernel;

    // start convolution
    for(i= 0; i < dataSizeY; ++i)                   // number of rows
    {
        // compute the range of convolution, the current row of kernel should be between these
        rowMax = i + kCenterY;
        rowMin = i - dataSizeY + kCenterY;

        for(j = 0; j < dataSizeX; ++j)              // number of columns
        {
            // compute the range of convolution, the current column of kernel should be between these
            colMax = j + kCenterX;
            colMin = j - dataSizeX + kCenterX;

            sum = 0;                                // set to 0 before accumulate

            // flip the kernel and traverse all the kernel values
            // multiply each kernel value with underlying input data
            for(m = 0; m < kernelSizeY; ++m)        // kernel rows
            {
                // check if the index is out of bound of input array
                if(m <= rowMax && m > rowMin)
                {
                    for(n = 0; n < kernelSizeX; ++n)
                    {
                        // check the boundary of array
                        if(n <= colMax && n > colMin)
                            sum += *(inPtr - n) * *kPtr;

                        ++kPtr;                     // next kernel
                    }
                }
                else
                    kPtr += kernelSizeX;            // out of bound, move to next row of kernel

                inPtr -= dataSizeX;                 // move input data 1 raw up
            }

            // convert negative number to positive
            *outPtr = (unsigned char)((float)fabs(sum) + 0.5f);

            kPtr = kernel;                          // reset kernel to (0,0)
            inPtr = ++inPtr2;                       // next input
            ++outPtr;                               // next output
        }
    }

    return true;
}
