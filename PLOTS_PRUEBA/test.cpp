#include "test.h"
#include "ui_test.h"
#include "tauudp.h"
#include "taunuc.h"
#include <QThread>
#include <QDebug>

Test::Test(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Test)
{
    ui->setupUi(this);

    QThread* thread2 = new QThread;
    TauNUC* workerNUC = new TauNUC();

    workerNUC->moveToThread(thread2);
//    connect(workerNUC, SIGNAL (error(QString)), this, SLOT (errorString(QString)));
    connect(thread2, SIGNAL (started()), workerNUC, SLOT (process()));
    connect(workerNUC, SIGNAL (finished()), thread2, SLOT (quit()));
    connect(workerNUC, SIGNAL (finished()), workerNUC, SLOT (deleteLater()));
    connect(thread2, SIGNAL (finished()), thread2, SLOT (deleteLater()));
    thread2->start();

    QThread* thread = new QThread;
    TauUDP* worker = new TauUDP();

    worker->moveToThread(thread);
//    connect(worker, SIGNAL (error(QString)), this, SLOT (errorString(QString)));
    connect(thread, SIGNAL (started()), worker, SLOT (process()));
    connect(worker, SIGNAL (finished()), thread, SLOT (quit()));
    connect(worker, SIGNAL (finished()), worker, SLOT (deleteLater()));
    connect(thread, SIGNAL (finished()), thread, SLOT (deleteLater()));
    thread->start();

    connect(worker, SIGNAL (newFrame(quint16 *)), workerNUC, SLOT (newFrame(quint16 *)));
    connect(workerNUC, SIGNAL (processedFrame(quint8 *)), this, SLOT (newFrame(quint8 *)));
}

Test::~Test()
{
    delete ui;
}

void Test::newFrame (quint8 * frame)
{
    uchar FrameIMG[512][640];

    memcpy ( FrameIMG, frame, 640*512 );

    QImage IMG = QImage(*FrameIMG, 640, 512, QImage::Format_Indexed8);
    ui->label->setPixmap(QPixmap::fromImage(IMG));

}
