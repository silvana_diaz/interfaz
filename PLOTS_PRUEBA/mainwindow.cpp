#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QThread"
#include "QString"
#include "QTextStream"
#include "QInputDialog"
#include <iostream>
#include <QFuture>
#include <QtCore>
#include <QtConcurrent/QtConcurrent>
#include <vector>
#include <registration.h>
#include <segmentation.h>
#include <classification.h>
#include <sys/time.h>
#include <QFuture>
#include <omp.h>
#include "test.h"
#include "tauudp.h"
#include "taunuc.h"
#include <QThread>
#include <QDebug>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>

//uint8_t showIR[FRAME_TAU_WIDTH*FRAME_TAU_HEIGHT*3];
uint16_t saveIR[FRAME_TAU_WIDTH*FRAME_TAU_HEIGHT*2];

int number_of_frames_input_video  = 3000;


QImage imgIn;
uint8_t * mat_otsu;

float theta[8]            = {1, 0, 0, 0, 1, 0, 0, 0};
float eta[3]              = {2e-5, 1e-2, 1e-10};

int corners[2][4];
int corners_flag = false;
CORNERS corners_rgb, corners_thm, c_assoc;
int c0x[4], c0y[4];

using namespace cv;
using namespace std;


QFile * framesIR;
QDataStream * inIR;

uchar matRGBFull[ROW][COL], matIRFull[FRAME_TAU_HEIGHT][FRAME_TAU_WIDTH];
Mat matIR, matRGB, matOut;
Mat matCv_norm, matCv_scaled;

//uint16_t showIR[FRAME_TAU_WIDTH*FRAME_TAU_HEIGHT];


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
  //  MainWindow::showFullScreen();
    setFixedSize(width(), height());

    timerReg = new QTimer(this);
    dataTimer = new QTimer(this);
    //ui->video_feed->label->setScaledContents(true);
    ui->image->setScaledContents(true);

    QPen redpen, bluepen; // se definen los colores de los graficos
    redpen.setColor(QColor(255,0,0));
    redpen.setWidthF(3);
    redpen.setStyle(Qt::SolidLine);
    bluepen.setColor(QColor(0,0,255));
    bluepen.setWidthF(3);
    bluepen.setStyle(Qt::SolidLine);

    ui->plot1->legend->setVisible(true);//se activan las leyendas
    QFont legendFont = font();
    legendFont.setPointSize(9);
    ui->plot1->legend->setFont(legendFont);
    ui->plot1->axisRect()->insetLayout()->setInsetAlignment(0,Qt::AlignBottom|Qt::AlignRight);
    ui->plot1->setInteraction(QCP::iRangeDrag, true);

    ui->plot1->addGraph();// agrega un grafico
    ui->plot1->graph(0)->setPen(bluepen);
    ui->plot1->graph(0)->setName("Lesión");

    ui->plot1->addGraph();//agrega otro grafico
    ui->plot1->graph(1)->setPen(redpen);
    ui->plot1->graph(1)->setName("NO-Lesión");

    ui->plot1->xAxis->ticker()->setTickCount(4);// ticks en el eje x
    ui->plot1->yAxis->ticker()->setTickCount(4);
    ui->plot1->replot();//"activa" graficos

    QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
    timeTicker->setTimeFormat("%m:%s");
    ui->plot1->xAxis->setTicker(timeTicker);
    ui->plot1->xAxis->setRange(0,120);
    ui->plot1->yAxis->setRange(-5, 255);
    ui->plainTextEdit->setReadOnly(true);


    ui->cuad1->setScaledContents(true);
    ui->cuad2->setScaledContents(true);
    ui->cuad3->setScaledContents(true);
    ui->cuad4->setScaledContents(true);

    connect(ui->video_feed,SIGNAL(numcuad(int)),this,SLOT(pinta_cuads(int)));
    connect(ui->video_feed,SIGNAL(getCorners()),this,SLOT(esquinas()));
    connect(timerReg, SIGNAL(timeout()), this, SLOT(updateVideo()));

    ui->segmentation->setText("Segmentation");
    ui->label->setWordWrap(true);

    ui->open->setStyleSheet("QPushButton {background-color: orange}");
/////////
    QThread* thread2 = new QThread;
    TauNUC* workerNUC = new TauNUC();

    workerNUC->moveToThread(thread2);
//    connect(workerNUC, SIGNAL (error(QString)), this, SLOT (errorString(QString)));
    connect(thread2, SIGNAL (started()), workerNUC, SLOT (process()));
    connect(workerNUC, SIGNAL (finished()), thread2, SLOT (quit()));
    connect(workerNUC, SIGNAL (finished()), workerNUC, SLOT (deleteLater()));
    connect(thread2, SIGNAL (finished()), thread2, SLOT (deleteLater()));
    thread2->start();

    QThread* thread = new QThread;
    TauUDP* worker = new TauUDP();

    worker->moveToThread(thread);
//    connect(worker, SIGNAL (error(QString)), this, SLOT (errorString(QString)));
    connect(thread, SIGNAL (started()), worker, SLOT (process()));
    connect(worker, SIGNAL (finished()), thread, SLOT (quit()));
    connect(worker, SIGNAL (finished()), worker, SLOT (deleteLater()));
    connect(thread, SIGNAL (finished()), thread, SLOT (deleteLater()));
    thread->start();

    connect(worker, SIGNAL (newFrame(quint16 *)), workerNUC, SLOT (newFrame(quint16 *)));
    connect(workerNUC, SIGNAL (processedFrame(quint8 *)), this, SLOT (newFrame(quint8 *)));
/////////

    gettimeofday(&oldcall, NULL);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::realtimeDataSlot()
{
    //se generan los puntos del grafico
    static QTime time(QTime::currentTime());
    double key = (time.elapsed()/1000.0);
    ////qDebug() << time.elapsed();
    QString s = QString::number(key);
    double x1 = 0, x2 = 0, y2 = 2, y1 = 1;
    //DATOS
   // cv::Scalar sum = cv::sum(frame);

   if (key > seg_rang)
   {
        ui->plainTextEdit->insertPlainText("PLOT FINISHED");
        on_stop_clicked();
        timerReg->stop();
        QString algorithm = "Euclidean";
            ////qDebug() << "hello " << lesion.size() << skin.size();
        QString result = classification(lesion, skin, algorithm);

        ui->label->setText(result);

        if (result.contains("Cancer"))
        {
            ui->label->setStyleSheet("QLabel { background-color : red; color : white; }");
        }
        else
        {
            ui->label->setStyleSheet("QLabel { background-color : cyan ; color : black; }");
        }


   }



   x1 = key;
   x2 = x1;
    //y1 = sum[0]/(camWidth*camHeight);
    //y2 = sum[1]/(camWidth*camHeight);

   /* QRgb rgb;
    rgb=qt_image.pixel(255,255);
    y1=qRed(rgb);
    y2=qBlue(rgb);*/
//    if(i==camHeight-1){
//        i=0;
//        ui->plainTextEdit->insertPlainText("End of frame\n");
//        ui->plainTextEdit->verticalScrollBar()->setValue(ui->plainTextEdit->verticalScrollBar()->maximum());
//    }
//    if(j==camWidth-1){
//        i++;
//        j=0;
//        ui->plainTextEdit->insertPlainText("End of line\n");
//        ui->plainTextEdit->verticalScrollBar()->setValue(ui->plainTextEdit->verticalScrollBar()->maximum());
//    }
//    j=j+1;

    y1 = tempVals.tempLesion;
    y2 = tempVals.tempSurround;

    qv_x.append(x1);
    qv_y.append(y1);
    qv_x2.append(x2);
    qv_y2.append(y2);

//    ui->plot1->graph(0)->addData(key, tempVals.tempLesion);
//    ui->plot1->graph(1)->addData(key, tempVals.tempSurround);

    plot();
    ui->statusBar->showMessage(
            QString("Total Data points: %1")
            .arg(qv_x.size()),0);

}

void MainWindow::plot()
{
    //actualiza los graficos
    ui->plot1->graph(0)->addData(qv_x, qv_y);
    ui->plot1->graph(1)->addData(qv_x, qv_y2);
    ui->plot1->replot();
    ui->plot1->update();

}

void MainWindow::clearData()
{
    //limpia los vectores
    qv_x.clear();
    qv_x2.clear();
    qv_y2.clear();
    qv_y.clear();
}


void MainWindow::on_stop_clicked()
{
    //para la simulacion
    ui->replot->setEnabled(true);
    disconnect(dataTimer, SIGNAL(timeout()), this, SLOT(realtimeDataSlot())); 

    timerReg->stop();
    QString algorithm = "Euclidean";
        ////qDebug() << "hello " << lesion.size() << skin.size();
    QString result = classification(lesion, skin, algorithm);

    ui->label->setText(result);

    if (result.contains("Cancer"))
    {
        ui->label->setStyleSheet("QLabel { background-color : red; color : white; }");
    }
    else
    {
        ui->label->setStyleSheet("QLabel { background-color : cyan ; color : black; }");
    }
    dataTimer->stop();
    dataTimer->destroyed();
    plot();
    ui->statusBar->showMessage(
        QString("Total Data points: %1")
        .arg(qv_x.size()),0);

}

void MainWindow::on_action_Save_As_triggered()
{
   //guarda los datos del grafico en un .txt
   QString filename = QFileDialog::getSaveFileName(this,
                        tr("Save TEMP Data"), "",
                        tr("Text File (*.txt);;All Files(*)"));

    if(filename.isEmpty())
        return;
    else{

        QFile file(filename);
        if(!file.open(QIODevice::WriteOnly)){
            QMessageBox::information(this, tr("Unable to open file"),
                                     file.errorString());
            return;
        }
        QTextStream out(&file);
        out << "Time\t";
        out << "TEMP_IN\t";
        out << "TEMP_OUT\t";
        out << "\n";
        QVector<double>::iterator itery = qv_y.begin();
        QVector<double>::iterator itery2 = qv_y2.begin();
        for(QVector<double>::iterator iter = qv_x.begin(); iter != qv_x.end(); iter++){
            out << *iter;
            out << "\t";
            out << *itery;
            out << "\t";
            out << *itery2;
            out << "\n";
            itery++;
            itery2++;
        }
        file.close();
    }
}

void MainWindow::on_restart_clicked()
{
    //reinicia la app.
    qApp->quit();
    QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
}

void MainWindow::on_replot_clicked()
{
    //ajusta grafico entero
    plot();
    ui->plot1->xAxis->setRange(*std::min_element(qv_x.begin(), qv_x.end())-1, *std::max_element(qv_x.begin(), qv_x.end()) + 2);
    ui->plot1->yAxis->setRange(-2, *std::max_element(qv_y.begin(), qv_y.end()) + 4);
    ui->plot1->replot();
    ui->plot1->update();
}

void MainWindow::on_saveplot_clicked()
{
    //para guardar imagen del grafico.
    QString filename = QFileDialog::getSaveFileName(this,
                         tr("Save Plot"), "",
                         tr("JPG (*.jpg);;All Files(*)"));
    if(filename.isEmpty())
        return;
    else{
        QFile file(filename);
        if(!file.open(QIODevice::WriteOnly)){
            QMessageBox::information(this, tr("Unable to open file"),
                                     file.errorString());
            return;
        }
        ui->plot1->saveJpg(filename, 0,0,1.0,-1);
    }
}

void MainWindow::on_open_clicked()
{
    //abre camara
    cap.open(0);
    cap.set(CV_CAP_PROP_FRAME_WIDTH, camWidth);//Resolucion
    cap.set(CV_CAP_PROP_FRAME_HEIGHT, camHeight);
    cap.set(CV_CAP_PROP_FPS,30);//probar otra camara, probar c200 en note y ver note.
    if(!cap.isOpened()){
        ui->plainTextEdit->insertPlainText("Could not open camera\n");
        ui->plainTextEdit->verticalScrollBar()->setValue(ui->plainTextEdit->verticalScrollBar()->maximum());
    }
    else{
        ui->plainTextEdit->insertPlainText("Camera Opened\n");
        ui->plainTextEdit->verticalScrollBar()->setValue(ui->plainTextEdit->verticalScrollBar()->maximum());
        connect(dataTimer,SIGNAL(timeout()), this, SLOT(update_window()));
        dataTimer->start(0);
        //frameTime.start();//timer para fps.
        ui->label->setText("2) Take Picture");
        ui->open->setStyleSheet("QPushButton {background-color: lightblue}");
        ui->snap->setStyleSheet("QPushButton {background-color: orange}");
    }

}

void MainWindow::update_window(){

    //cuadros de video
    cap >> frame;
    cv::cvtColor(frame,frame,CV_BGR2RGB); //al compilar generaba un error;
    qt_image=QImage((const unsigned char*) (frame.data),frame.cols,frame.rows, QImage::Format_RGB888);


    //ui->video_feed->label->setPixmap(QPixmap::fromImage(qt_image));
    ui->video_feed->setPixmap(QPixmap::fromImage(qt_image));
    /*framecount++;
    if(frameTime.elapsed()>1000)
        fps = framecount / (double) (frameTime.elapsed()/1000.0);

    ui->statusBar->showMessage(QString("Cam FPS: %1").arg((int)fps),0);
*/
}

void MainWindow::on_close_clicked()
{
    //desconección de camara
    disconnect(dataTimer, SIGNAL(timeout()),this,SLOT(update_window()));
    if(cap.isOpened())
        cap.release();
    dataTimer->stop();
    cv::Mat image = cv::Mat::zeros(frame.size(), CV_8UC3);//label en negro
    qt_image=QImage((const unsigned char*) (image.data),image.cols,image.rows, QImage::Format_RGB888);

    ui->video_feed->label->setPixmap(QPixmap::fromImage(qt_image));
    ui->plainTextEdit->appendPlainText("Camera Closed\n");
    ui->plainTextEdit->verticalScrollBar()->setValue(ui->plainTextEdit->verticalScrollBar()->maximum());
}

void MainWindow::on_snap_clicked()
{

//    framecount = 0;
//    second_video = true;
//    cap >> frame_rgb;
//    cv::cvtColor(frame_rgb,frame_rgb,CV_BGR2RGB); //al compilar generaba un error;
//    qt_image_copy=QImage((const unsigned char*) (frame_rgb.data),frame_rgb.cols,frame_rgb.rows, QImage::Format_RGB888);

    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Image"), "", tr("Image Files (*.png *.jpg *.jpeg *.bmp)"), nullptr, QFileDialog::DontUseNativeDialog);

   // QString fileName = "/home/silvana/Documentos/im22.jpg";
    img.load(fileName);
    qt_image_copy= img;

    if(qt_image_copy.isNull())
    {
        ui->plainTextEdit->insertPlainText("Unable to take picture\n");
        ui->plainTextEdit->verticalScrollBar()->setValue(ui->plainTextEdit->verticalScrollBar()->maximum());
    }
    else
    {
        qt_image = qt_image_copy;

        ui->video_feed->label->setPixmap(QPixmap::fromImage(qt_image_copy));
        //qt_image.save("/home/pi/Desktop/pruebaRPI.jpg");
        disconnect(dataTimer,SIGNAL(timeout()),this,SLOT(update_window()));
        //dataTimer->stop();
        //dataTimer->destroyed();
        //if(dataTimer->isActive())
        //ui->plainTextEdit->insertPlainText("Timer is still opened.\n");
        ui->plainTextEdit->insertPlainText("Picture Taken\n");
        ui->plainTextEdit->verticalScrollBar()->setValue(ui->plainTextEdit->verticalScrollBar()->maximum());
        cap.release();
        ui->label->setText("3) Select 4 corner ROIs in RGB");
        ui->snap->setStyleSheet("QPushButton {background-color: lightblue}");
        ui->segmentation->setStyleSheet("QPushButton {background-color: orange}");
    }
}


void MainWindow::on_addData_clicked()
{
    ui->plainTextEdit->insertPlainText("\nStart Graphics\n");
    ui->plainTextEdit->verticalScrollBar()->setValue(ui->plainTextEdit->verticalScrollBar()->maximum());
    bool ok;
    seg_rang= QInputDialog::getInt(this,tr("Ingresar Segundos"),tr("Segundos:"),
                                       120,0,1000,1, &ok );
    ui->plot1->xAxis->setRange(0, seg_rang);
         dataTimer->start(0);

    ui->plot1->replot();
    connect(dataTimer, SIGNAL(timeout()), this, SLOT(realtimeDataSlot()));//empieza el grafico.
    //timer->start(0);
}


void MainWindow::on_redraw_clicked()
{
    if (!IR)
    {
        pts_src.clear();
    }
    ui->label->setText("Corners have been removed.");
    ui->label->setText("3) Select 4 corner ROIs in RGB");
    ui->video_feed->enter = 0;
    ui->video_feed->setPixmap(QPixmap::fromImage(qt_image_copy));
    pts_dst.clear();
    connect(ui->video_feed,SIGNAL(numcuad(int)),this,SLOT(pinta_cuads(int)));
    connect(ui->video_feed,SIGNAL(getCorners()),this,SLOT(esquinas()));
}

void MainWindow::pinta_cuads(int num)
{
    QImage cuad = qt_image_copy.copy(ui->video_feed->mRect);
    if(num == 1)
        ui->cuad1->setPixmap(QPixmap::fromImage(cuad));
    else if(num == 2)
        ui->cuad2->setPixmap(QPixmap::fromImage(cuad));
    else if(num == 3)
        ui->cuad3->setPixmap(QPixmap::fromImage(cuad));
    else if(num == 4)
        ui->cuad4->setPixmap(QPixmap::fromImage(cuad));

 //   ui->label->setText("4) Start Segmentation step 1");
}

void MainWindow::esquinas()
{
    if (!IR)
    {
        ////qDebug() << "HCD RGB";

        posROIs.LL = &ui->video_feed->posLeftDown[0];
        posROIs.LR = &ui->video_feed->posRightDown[0];
        posROIs.UL = &ui->video_feed->posLeftUp[0];
        posROIs.UR = &ui->video_feed->posRightUp[0];

        esq = RGB_Harris(qt_image_copy, posROIs);

        qim = QPixmap::fromImage(qt_image_copy);
        qPainter.begin(&qim);
        qPainter.setBrush(Qt::NoBrush);
        qPainter.setPen(QPen(QColor(0, 255, 0), 2, Qt::SolidLine, Qt::SquareCap, Qt::BevelJoin));

        for (unsigned int i = 0; i < 4; i++)
        {
            qPainter.drawRect(c_assoc.c[i].x0-5,c_assoc.c[i].x1-5,10,10);
        }
        qPainter.end();

        ui->video_feed->label->setPixmap(qim);

        ui->label->setText("If necessary, you can redraw the corners by pressing the Reset Draw button. If not, press the Segmentation button.");
//        QMessageBox msgBox;
//        msgBox.setWindowTitle("title");
//        msgBox.setText("Question");
//        msgBox.setStandardButtons(QMessageBox::Yes);
//        msgBox.addButton(QMessageBox::No);
//        msgBox.setDefaultButton(QMessageBox::No);
//        if(msgBox.exec() == QMessageBox::Yes)
//        {
//          // do something
//        }else {
//          // do something else
//        }

        //disconnect(ui->video_feed,SIGNAL(numcuad(int)),this,SLOT(pinta_cuads(int)));
        //disconnect(ui->video_feed,SIGNAL(getCorners()),this,SLOT(esquinas()));
    }
    else
    {
        //connect(ui->video_feed,SIGNAL(numcuad(int)),this,SLOT(pinta_cuads(int)));
        //connect(ui->video_feed,SIGNAL(getCorners()),this,SLOT(esquinas()));
        ////qDebug() << "HCD IR";


        IRframes();
    }
}

void MainWindow::on_segmentation_clicked()
{
    ui->label->setText("4) Click on the lesion.");
    ui->video_feed->label->setScaledContents(false);
    ui->plainTextEdit->insertPlainText("Starting Segmentation\n");
    ui->plainTextEdit->verticalScrollBar()->setValue(ui->plainTextEdit->verticalScrollBar()->maximum());
   // connect(ui->video_feed,SIGNAL(pressed(QPoint)),this,SLOT(get_otsuclick_pos(QPoint)));

    generateMask(qt_image_copy, esq);

    ui->video_feed->otsuReady = true;
    ui->image->setPixmap(qim);
    QImage imgSeg= QImage((const unsigned char*)  mat_otsu,  miniCol, miniRow, miniCol, QImage::Format_Grayscale8);
    imgSeg.save("/home/silvana/Documentos/imSeg.jpg");
    ui->video_feed->setPixmap(QPixmap::fromImage(imgSeg));

    ui->plainTextEdit->insertPlainText("Select the lesion. \n");
    connect(ui->video_feed, SIGNAL(seeds(int, int)), this, SLOT(clickSegm(int,int)));

//    if (ui->video_feed->segmReady)
//    {
//        //qDebug() << "ruaa";
//        QImage matimg= QImage((const unsigned char*)  dstMask.data,  dstMask.cols, dstMask.rows,  dstMask.cols, QImage::Format_Grayscale8);
//        ui->video_feed->setPixmap(QPixmap::fromImage(matimg));

//        ui->plainTextEdit->insertPlainText("Skin identified\n");
//        ui->plainTextEdit->verticalScrollBar()->setValue(ui->plainTextEdit->verticalScrollBar()->maximum());
//        ui->label->setText("5) Start IR camera");
//        ui->segmentation->setStyleSheet("QPushButton {background-color: lightblue}");
//        ui->startIR->setStyleSheet("QPushButton {background-color: orange}");
//    }
}
void MainWindow::onSegmReady()
{

    ui->video_feed->otsuReady = false;

    QImage matimg= QImage((const unsigned char*)  dstMask.data,  dstMask.cols, dstMask.rows,  dstMask.cols, QImage::Format_Grayscale8);
    ui->video_feed->setPixmap(QPixmap::fromImage(matimg));

    ui->plainTextEdit->insertPlainText("Skin identified\n");
    ui->plainTextEdit->verticalScrollBar()->setValue(ui->plainTextEdit->verticalScrollBar()->maximum());
    ui->label->setText("5) Start IR camera");
    ui->segmentation->setStyleSheet("QPushButton {background-color: lightblue}");
    ui->startIR->setStyleSheet("QPushButton {background-color: orange}");
}

void MainWindow::get_otsuclick_pos(QPoint click)
{
    posx = click.x();
    posy = click.y();
}

void MainWindow::on_startIR_clicked()
{
//    ui->plainTextEdit->insertPlainText("Starting IR camera\n");
//    ui->plainTextEdit->verticalScrollBar()->setValue(ui->plainTextEdit->verticalScrollBar()->maximum());

//    // Configuracion UDP
//    socket = new QUdpSocket(this);
//    socket->bind(QHostAddress(quint32(0)), 1234);
//    connect(socket, SIGNAL(readyRead()), this, SLOT(ReadUDP()));

//    // IP de destino
//    IPDest.setAddress("169.254.0.255");

//    // Scrollbar al final de area miniaturas
//    //QScrollBar* scrollbar = ui->LastsImages->horizontalScrollBar();
//    //QObject::connect(scrollbar, SIGNAL(rangeChanged(int,int)), this, SLOT(moveScrollBarToEnd(int, int)));

//    // Valoers iniciales
//    last_min        = 0;
//    last_max        = 3000;
//    line_counter    = 0;
//    RecordVideo     = false;
//    DatabaseMode    = false;
//    StreamOn        = true;

//    // Tamaño inicial de ventana
//    //ui->LastsImages->hide();
//    //this->resize(773, 535);

    //filename="/home/silvana/Documentos/P74.raw";

    // Download calibrate data 25 deg C
    FILE *fp= fopen("0.bin", "rb");
    if(fp != nullptr){
      fread(image_cal, 2, 384*288, fp);
      fclose(fp);
    }

    therm = thermapp_initUSB();
    if(therm == nullptr) {
        //qDebug()<< "init Error";
        return;
    }

    if(thermapp_USB_checkForDevice(therm, VENDOR, PRODUCT) == -1)
    {
       //qDebug()<< "USB_checkForDevice Error";
       return;
    }
    else
    {
        //qDebug()<< "thermapp_FrameRequest_thread";
        //Run thread usb therm
        thermapp_FrameRequest_thread(therm);
    }

    /// Run frame refresh timer
    frameTimer = new QTimer(this);
  //  connect(frameTimer, SIGNAL(timeout()), this, SLOT(UpdateProcessFrame()));

    QString fileName = "/home/silvana/Vídeos/AmatD.bin";
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(nullptr, "error", file.errorString());
    }

    QDataStream in(&file);

    while (!file.atEnd())
    {
        in.readRawData((char * ) ganancia, PIXELS_DATA_SIZE*sizeof(double));
    }
    file.close();

    QString fileName2 = "/home/silvana/Vídeos/BmatD.bin";
    QFile file2(fileName2);
    if(!file2.open(QIODevice::ReadOnly)) {
        QMessageBox::information(nullptr, "error", file2.errorString());
    }

    QDataStream in2(&file2);

    while (!file2.atEnd())
    {
        in2.readRawData((char * ) offset, PIXELS_DATA_SIZE*sizeof(double));
    }

    file2.close();

    temp_prev = thermapp_getTemperature(therm);
    count = 0;

//    gainCal = 2;
//    ui->gain->setText(QString("%1").arg(1));
//    offsetCal = 255;
//    ui->offset->setText(QString("%1").arg(0));

    ui->video_feed->ir = true;
    ui->video_feed->enter = 0;
    IR = true;
    ui->label->setText("6) Select 4 corner ROIs in IR");


    qDebug() << "end startIR_click";


    getVideo();
   // QImage matimg= QImage((const unsigned char*)  dstMask.data,  dstMask.cols, dstMask.rows,  dstMask.cols, QImage::Format_Grayscale8);
   // ui->image->setPixmap(QPixmap::fromImage(matimg));
}

//FIXME: This method transfers RAW data from ThermApp to QPixmap
QPixmap MainWindow::Frame(short *frame)
{
   //qDebug() << "FRAME IR";
   int pix_lim, i, tmp = 0, tmp0 = 0;
   //double pix;
   // unsigned short gain = (unsigned short)thermapp_getGain(therm);
   //int i_f=0;
   int i_p = 0;

//   uchar frame_trans[PIXELS_DATA_SIZE];

    for(i = 0; i < PIXELS_DATA_SIZE; i++)
    {
        frame_trans[i] = (frame[i]- offset[i]) / ganancia[i];//image_cal[i]) * gainCal) + offsetCal;
    }

 // pair min_max ;
 // getMinMax(&min_max, frame_trans, PIXELS_DATA_SIZE);
  //ui->labelDinamic->setText(QString("Dynamic range: %1").arg(sqrt((min_max.max - min_max.min) * (min_max.max - min_max.min))));
  //ui->minmaxlabel->setText(QString("max: %1, min: %2").arg(min_max.max).arg(min_max.min));
    for(i = 0; i < PIXELS_DATA_SIZE; i++){

        pix_lim = frame_trans[i];// * gainCal;


        ////qDebug() << pix_lim;
        if((pix_lim > 255) && (tmp == 0)){
            tmp = 1;
            ////qDebug() << "overflow > " << i << pix_lim;
            //int agc = ui->gainCalSlider->value();
            //agc -= 50;
            //ui->gainCalSlider->setValue(agc);

        }

        if((pix_lim < 0) && (tmp0 == 0)){
            tmp0 = 1;
            //int agc = ui->offsetCalSlider->value();
            ////qDebug() << "overflow < "<< i << pix_lim;
            //agc += sqrt(min_max.min * min_max.min);
            //ui->offsetCalSlider->setValue(agc);
        }


        if(pix_lim > 255)
            pix_lim = 255;

        if(pix_lim < 0){
            ////qDebug() << "overflow <";
            pix_lim = 0;
        }
        imageData[i_p] = (unsigned char)pix_lim;
        i_p++;
        imageData[i_p] = (unsigned char)pix_lim;
        i_p++;
        imageData[i_p] = (unsigned char)pix_lim;
        i_p++;
    }

    QPixmap pixmap = QPixmap::fromImage(
        QImage(
            (unsigned char *) imageData,
            384,
            288,
            QImage::Format_RGB888
                   // ,QImage::Format_Mono
        )
    );

    QImage lalal = QImage((const unsigned char*)  imageData, FRAME_TAU_HEIGHT, FRAME_TAU_WIDTH, QImage::Format_RGB888);
    lalal.save("/home/silvana/Documentos/showIR.jpg");

    int gray;

//    //qDebug() << "size" << lalal.size() << lalal.width() << FRAME_TAU_WIDTH << lalal.height() << FRAME_TAU_HEIGHT;
//    return pixmap;

    for (int i = 0; i < FRAME_TAU_WIDTH; i++)
    {
        for (int j = 0; j < FRAME_TAU_HEIGHT; j++)
        {
            gray = qGray((lalal.pixel(j, i)));
            lalal.setPixel(j, i, QColor(gray, gray, gray).rgb());
            matIR[j + i * FRAME_TAU_WIDTH] = gray;
        }
    }


    return pixmap.transformed(QTransform().scale(-1, 1));
}

void MainWindow::UpdateProcessFrame()
{
    short frame[PIXELS_DATA_SIZE];
//    thermapp_GetImage(therm, frame);



//    QImage d = QImage((unsigned char *) data, 384, 288, QImage::Format_RGB888);
//    ui->video_feed->setPixmap(QPixmap::fromImage(d));
    if(!thermapp_GetImage(therm, frame))
       return;
       qt_image_copy =  Frame(frame).toImage().mirrored(true, false);

//    //qDebug() << "qt_image size" << qt_image_copy.size();

//       qt_image_copy = QImage((unsigned char *) imageData, 384, 288, QImage::Format_RGB888); //QImage((const unsigned char*)  frame, 384, 288, 384, QImage::Format_Grayscale8);
//       ui->video_feed->setPixmap(QPixmap::fromImage(qt_image_copy));
       qt_image_copy.save("/home/silvana/Documentos/qt_image_copy.jpg");


//       int gray;
//        //qDebug() << qt_image_copy.size();

//       for (int i = 0; i < 384; i++)
//       {
//           for (int j = 0; j < 288; j++)
//           {
//               gray = qGray((qt_image_copy.pixel(j, i)));
//               //qt_image_copy.setPixel(j, i, QColor(gray, gray, gray).rgb());
//               data[i*FRAME_TAU_HEIGHT + j] = gray;

//               //qDebug() <<  i << j;
//           }
//       }
//    }
//    ////qDebug() << "esydut";

//        QByteArray bytes;
//        QBuffer buffer(&bytes);
//        buffer.open(QIODevice::WriteOnly);
//        qt_image_copy.save(&buffer, "PNG");
        //buffer.close();

//        std::vector<uchar> c(bytes.constData(), bytes.constData() + bytes.size());

    //    data = (unsigned char *) malloc(bytes.size());
        //memcpy(data, reinterpret_cast<unsigned char *>(bytes.data()), bytes.size());
       // QImage imgIn = QImage((const unsigned char*)  data, 384,288,384, QImage::Format_Indexed8);
        //imgIn.save("/home/silvana/Documentos/imgIn.jpg");
//    for (uint i = 0; i < FRAME_TAU_HEIGHT*FRAME_TAU_WIDTH; i++)
//    {
//        showIR[i] = imageData[i];
//    }

//    matIR = Mat(FRAME_TAU_HEIGHT, FRAME_TAU_WIDTH, CV_8UC1, imageData);
//    normalize(matIR, matCv_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat());
//    convertScaleAbs(matCv_norm, matCv_scaled);

//    QImage imgIn = QImage((const unsigned char*)  matCv_scaled.data, matCv_scaled.cols, matCv_scaled.rows, matCv_scaled.step, QImage::Format_Grayscale8);
    raro = QPixmap::fromImage(qt_image_copy);
    IR = true;
//    QImage frames = QImage((unsigned char*) showIR, FRAME_TAU_WIDTH, FRAME_TAU_HEIGHT, \
//                                  FRAME_TAU_WIDTH, QImage::Format_Indexed8);

    ui->video_feed->setPixmap(raro);


}


void MainWindow::getVideo()
{
    QTime getVid;
    getVid.start();

    ui->video_feed->enter = 0;

    frames++;
    ////qDebug() << "FRAMES: " << frames;

    //QFile * framesIR;

    if (firstframe)
    {
        UpdateProcessFrame();

//        short frame[PIXELS_DATA_SIZE];
//        double d_frame[PIXELS_DATA_SIZE];

//        while(!thermapp_GetImage(therm, frame));

//        for(int i = 0; i < PIXELS_DATA_SIZE; i++)
//        {
//            d_frame[i] = frame[i];
//        }

//        for(int i = 0; i < 50; i++)
//        {
//            while(!thermapp_GetImage(therm, frame));

//            for(int j = 0; j < PIXELS_DATA_SIZE; j++)
//            {
//                d_frame[j] += frame[j];
//            }
//        }

//        for(int i = 0; i < PIXELS_DATA_SIZE; i++)
//        {
//             image_cal[i] = d_frame[i] / 50;
//        }

        while(!therm->is_NewFrame);
//        QThread::sleep(5);

        UpdateProcessFrame();

        //frameTimer->start(100);
        //framesIR = new QFile(filename);
        //inIR = new QDataStream(framesIR);
        //framesIR->open(QIODevice::ReadOnly);
        //videoOver = false;
        ////qDebug() << "openVideo";
    }

//    inIR->readRawData((char*) saveIR, FRAME_TAU_WIDTH*FRAME_TAU_HEIGHT*2);

//    if (inIR->atEnd() || frames == 1000)
//    {
//        //qDebug() << "END VIDEO";
//        videoOver = true;
//        timerReg->stop();

//        number_of_frames_input_video = frames - 1;
//        //qDebug() << "number_of_frames_input_video" << frames;
//        QString algorithm = "CommTheory01";
//            ////qDebug() << "hello " << lesion.size() << skin.size();
//        QString result = classification(lesion, skin, algorithm);

//        ui->label->setText(result);
//        return;
//    }

//    uint16_t max = saveIR[0];
//    uint16_t min = saveIR[0];

//    for (int i = 0; i < FRAME_TAU_HEIGHT*FRAME_TAU_WIDTH; i++)
//    {
//        if (saveIR[i] > max)
//        {
//          max = saveIR[i];
//        }
//        else if (saveIR[i] < min)
//        {
//          min = saveIR[i];
//        }
//    }

//    if (firstframe)
//    {
//        min = 8098;
//        max = 10831;
//    }
//    for (int i = 0; i < FRAME_TAU_WIDTH*FRAME_TAU_HEIGHT; i++)
//    {
//        showIR[i] = (saveIR[i] - min)/float(max - min) * 255;
//    }

//    matIR = Mat(FRAME_TAU_HEIGHT, FRAME_TAU_WIDTH, CV_8UC1, showIR);
//    normalize(matIR, matCv_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat());
//    convertScaleAbs(matCv_norm, matCv_scaled);


//    if (firstframe)
//    {
//        ui->video_feed->resize( FRAME_TAU_WIDTH, FRAME_TAU_HEIGHT );
//        QImage frame = QImage((unsigned char*) showIR, FRAME_TAU_WIDTH, FRAME_TAU_HEIGHT, \
//                              FRAME_TAU_WIDTH, QImage::Format_Indexed8);
//        ui->video_feed->setPixmap(QPixmap::fromImage(frame));
//        qt_image_copy = frame;
//    }

////    QImage frame = QImage((unsigned char*) showIR, FRAME_TAU_WIDTH, FRAME_TAU_HEIGHT, \
////                          FRAME_TAU_WIDTH, QImage::Format_Indexed8);

//    QImage imgIn = QImage((const unsigned char*)  matCv_scaled.data, matCv_scaled.cols, matCv_scaled.rows, matCv_scaled.step, QImage::Format_Grayscale8);
//    raro = QPixmap::fromImage(imgIn);
//  //  qt_image_copy = frame;
//    IR = true;

    ////qDebug() << "getVideo time: " << getVid.elapsed();
}


////////////////////////////////////////////////////////////////////////////////////////
/************************** HARRIS CORNER DETECTOR - SILVANA **************************/
////////////////////////////////////////////////////////////////////////////////////////
void MainWindow::getImage(QImage& image, uchar matRGBFull[ROW][COL])
{
    int gray;

    for (int i = 0; i < ROW; i++)
    {
        for (int j = 0; j < COL; j++)
        {
            gray = qGray((image.pixel(j, i)));
            image.setPixel(j, i, QColor(gray, gray, gray).rgb());
            matRGBFull[i][j] = gray;
        }
    }

    matRGB = Mat(ROW, COL, CV_8UC1, matRGBFull);
    normalize(matRGB, matCv_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat());
    convertScaleAbs(matCv_norm, matCv_scaled);
}

//Needs:
// - An RGB frame
// - The positions of the ROI in a structure.
CORNERS MainWindow::RGB_Harris(QImage& img, IRHarrisOutputs positionsROI)
{
   // //qDebug() << "RGB Harris";
    CORNERS ret;
    /* Convert RGB image to grayscale */

    if (pts_src.size()> 0)
        pts_src.clear();
    getImage(img, matRGBFull); //102 ms

    for (int num = 0; num < 4; num++)
    {
        int beginCol = positionsROI.UL[num].x();
        int endCol   = positionsROI.UR[num].x();
        int beginRow = positionsROI.UL[num].y();
        int endRow   = positionsROI.LL[num].y();

        //if (beginCol == 0) return;

        int row_len = endRow - beginRow;
        int col_len = endCol - beginCol;

        uchar miniMat[row_len][col_len];

        for (int i = beginRow; i < endRow; i++)
        {
            for (int j = beginCol; j < endCol; j++)
            {
                miniMat[i-beginRow][j-beginCol] = matRGBFull[i][j];
            }
        }


        Mat src, dst, dst_norm, dst_norm_scaled;
        src = Mat(row_len, col_len, CV_8UC1, miniMat);
        dst = Mat::zeros(src.size(), CV_8UC1);

        cornerHarris(src, dst, 5, 3, 0.04, BORDER_DEFAULT);
        normalize(dst, dst_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat());
        convertScaleAbs(dst_norm, dst_norm_scaled);

        double min, max;
        Point minLoc, maxLoc;
        minMaxLoc(dst_norm, &min, &max, &minLoc, &maxLoc);

        int i = maxLoc.x;
        int j = maxLoc.y;

        corners_rgb.c[num].x0 = i+beginCol;
        corners_rgb.c[num].x1 = j+beginRow;

        pts_src.push_back(Point2f(corners_rgb.c[num].x0, corners_rgb.c[num].x1));
    }

    c_assoc = corners_rgb;


    return c_assoc;
}

//Needs:
// - An RGB frame.
// - Corners of the RGB image.
// - Global mat_otsu
void MainWindow::generateMask(QImage img, CORNERS cRGB)
{
    min_corner_x = minCorners(cRGB.c[3].x0, cRGB.c[2].x0, cRGB.c[1].x0, cRGB.c[0].x0) + 10;
    max_corner_x = maxCorners(cRGB.c[3].x0, cRGB.c[2].x0, cRGB.c[1].x0, cRGB.c[0].x0) - 10;
    min_corner_y = minCorners(cRGB.c[3].x1, cRGB.c[2].x1, cRGB.c[1].x1, cRGB.c[0].x1) + 10;
    max_corner_y = maxCorners(cRGB.c[3].x1, cRGB.c[2].x1, cRGB.c[1].x1, cRGB.c[0].x1) - 10;
    miniRow =  max_corner_y - min_corner_y;
    miniCol =  max_corner_x - min_corner_x;

    uint8_t mat[max_corner_y - min_corner_y][max_corner_x - min_corner_x][3];

    for (int i = min_corner_y; i < max_corner_y; i++)
    {
        for (int j = min_corner_x; j < max_corner_x; j++)
        {
            mat[(i-min_corner_y)][j-min_corner_x][0] = qRed(img.pixel(j,i));
            mat[(i-min_corner_y)][j-min_corner_x][1] = qGreen(img.pixel(j,i));
            mat[(i-min_corner_y)][j-min_corner_x][2] = qBlue(img.pixel(j,i));
        }
    }

    Mat matIn = Mat(miniRow, miniCol, CV_8UC3, mat);
    medianBlur(matIn, matOutSeg, 5);
    mat_otsu = (uint8_t *) calloc (miniRow * miniCol, sizeof(uint8_t));

    if (mat_otsu == NULL)
    {
        return;
    }

    multithresholding((uint8_t * ) mat_otsu, (uint8_t * ) matOutSeg.data, miniRow, miniCol, false, 0);

    QImage matimg= QImage((const unsigned char*)  mat_otsu,  miniCol, miniRow, miniCol, QImage::Format_Grayscale8);
    matimg.save("/home/silvana/Documentos/otsu.jpg");
}

// - Click position in x (x,y).
// - Click position in y (x,y).
maskLists MainWindow::clickSegm(int posx, int posy)
{
    int nLabels;
    int valMask;

    uint8_t * matF =  (uint8_t *) calloc (miniRow * miniCol, sizeof(uint8_t));
    valMask = * (mat_otsu + (posx) + (posy)*miniCol);

    binarization((uint8_t * ) matF, (uint8_t * ) mat_otsu, miniRow, miniCol, valMask);

    uint8_t * matErode;
    uint8_t * matDilate;
    matErode  = (uint8_t * ) calloc(miniRow * miniCol, sizeof(uint8_t));
    matDilate = (uint8_t * ) calloc(miniRow * miniCol, sizeof(uint8_t));

    erode(matF, miniCol, miniRow, matErode, 5);
    dilate(matErode, miniCol, miniRow, matDilate, 5);


    Mat matLabel = Mat(miniRow, miniCol, CV_8UC1, matDilate);
    Mat matLabelGood = (matLabel == 0); // Necessary but no idea why. It does not work with matLabel alone

    matComp = Mat::zeros(matLabel.size(), CV_32S);
    nLabels = connectedComponents(matLabelGood, matComp , 8);

    int val = matComp.at<int>(posy, posx);

    lesionList.clear();
    skinList.clear();
    dstMask = Mat::zeros(matComp.size(), CV_8UC1);

    for(int r = 0; r < miniRow; ++r)
    {
        for(int c = 0; c < miniCol; ++c)
        {
            if (matComp.at<int>(r,c) == val) //this needs to be clicked
            {
                dstMask.at<uchar>(r,c) = 255;
                lesionList.append(QPoint(c + min_corner_x, r + min_corner_y));
            }
            else
                skinList.append(QPoint(c + min_corner_x, r + min_corner_y));
         }
    }
    maskLists lists;
    lists.lesionList = lesionList;
    lists.skinList = skinList;

    onSegmReady();

    return lists;
}

void MainWindow::IR_Harris(uchar showIR[FRAME_TAU_WIDTH*FRAME_TAU_HEIGHT], int cornerID, IRHarrisOutputs &outputs)
{
    //qDebug() << "IR Harris" << cornerID;

    int beginCol = outputs.UL->x();
    int endCol   = outputs.UR->x();
    int beginRow = outputs.UL->y();
    int endRow   = outputs.LL->y();

    //if (beginCol == 0) return;

    int row_len = endRow - beginRow;
    int col_len = endCol - beginCol;

    uchar miniMat[row_len][col_len];

    for (int i = beginRow; i < endRow; i++)
    {
        for (int j = beginCol; j < endCol; j++)
        {
            miniMat[i-beginRow][j-beginCol] = matIR[j + i * FRAME_TAU_WIDTH];
        }
    }

    QImage mini = QImage((const unsigned char*)  miniMat, col_len, row_len, col_len, QImage::Format_Grayscale8);
    mini.save("/home/silvana/Documentos/miniMat.jpg");

    Mat src, dst, dst_norm, dst_norm_scaled;
    src = Mat(row_len, col_len, CV_8UC1, miniMat);
    dst = Mat::zeros(src.size(), CV_8UC1);

    cornerHarris(src, dst, 5, 3, 0.04, BORDER_DEFAULT);
    normalize(dst, dst_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat());
    convertScaleAbs( dst_norm, dst_norm_scaled );

    double min, max;
    Point minLoc, maxLoc;
    minMaxLoc(dst_norm, &min, &max, &minLoc, &maxLoc);

    int i = maxLoc.x;
    int j = maxLoc.y;

//    if (corners_flag)
//    {
//        if (abs((i+beginCol - corners_thm.c[cornerID].x0)) > (outputs.LR[cornerID].x() - outputs.UL[cornerID].x())/4 || abs((j + beginRow - corners_thm.c[cornerID].x1)) > (outputs.LL[cornerID].y() - outputs.UL[cornerID].y())/4)
//        {
//            i = corners_thm.c[cornerID].x0 - beginCol;
//            j = corners_thm.c[cornerID].x1 - beginRow;
//        }
//    }

    corners_thm.c[cornerID].x0 = i+beginCol;
    corners_thm.c[cornerID].x1 = j+beginRow;

    if (cornerFlag && cornerID == 3)
    {
        cornerFlag = false;
        for (int i = 0; i < 4; i++)
        {
            pts_dst.push_back(Point2f(corners_thm.c[i].x0, corners_thm.c[i].x1));
        }
    }

    //qDebug() <<  "corners" << corners_thm.c[cornerID].x0 << corners_thm.c[cornerID].x1;

    ui->startIR->setStyleSheet("QPushButton {background-color: lightblue}");
    outputs.cThm = corners_thm;
}

//Needs:
// - A infrared video frame.
// - A number identifying the ROI where the corner will be found.
// - The positions of the ROI in a structure.
CORNERS MainWindow::firstFrame(uchar showIR[FRAME_TAU_WIDTH*FRAME_TAU_HEIGHT], int cornerID, IRHarrisOutputs outputs)
{
    //qDebug() << "firtsFrame()";
    outputs.LL = &ui->video_feed->posLeftDown[cornerID];
    outputs.LR = &ui->video_feed->posRightDown[cornerID];
    outputs.UL = &ui->video_feed->posLeftUp[cornerID];
    outputs.UR = &ui->video_feed->posRightUp[cornerID];

    IR_Harris(showIR, cornerID, outputs);

  //  for (int i = 0; i < ui->video_feed->enter; ++i)
    {
   //     updateROI(&outputs.cThm.c[cornerID], outputs.UL, outputs.UR, outputs.LL, outputs.LR, true);
    }

    return outputs.cThm;
}

void MainWindow::IRframes()
{
    IRHarrisOutputs outputs;

    outputs.UR = &posRightUp[ui->video_feed->enter - 1];
    outputs.UL = &posLeftUp[ui->video_feed->enter - 1];
    outputs.LR = &posRightDown[ui->video_feed->enter - 1];
    outputs.LL = &posLeftDown[ui->video_feed->enter - 1];
    //qDebug() << "IRframes()";

    //QImage lalal = QImage((const unsigned char*)  imageData, FRAME_TAU_HEIGHT, FRAME_TAU_WIDTH, QImage::Format_RGB888);
    //lalal.save("/home/silvana/Documentos/imageData.jpg");

    //qDebug() << "enter" << ui->video_feed->enter;

    for (int i = 0; i < ui->video_feed->enter; i++)
    {
        esq = firstFrame(matIR, i, outputs);
    }

    //QPixmap qim = QPixmap::fromImage(qt_image_copy);
    QPixmap qim = QPixmap(ui->video_feed->Pixmap());
    qPainter.begin(&qim);
    qPainter.setBrush(Qt::NoBrush);
    qPainter.setPen(QPen(QColor(0, 255, 0), 2, Qt::SolidLine, Qt::SquareCap, Qt::BevelJoin));

    for (int i = 0; i <  ui->video_feed->enter; i++)
    {
        qPainter.drawRect(esq.c[i].x0-5, esq.c[i].x1-5, 10, 10);
    }

    qPainter.end();
    ui->label->setText("7) Start registration.");

    ui->video_feed->label->setPixmap(qim);
}

void MainWindow::updateVideo()
{
    ui->label->setText("8) Press on Start Plot to obtain the TRC.");
    //getVideo();
    UpdateProcessFrame();
    ////qDebug() << "UpdateVideo";

    COORDS coords;

    qPainter.begin(&raro);
    qPainter.setBrush(Qt::NoBrush);
    qPainter.setPen(QPen(QColor(0, 255, 0), 2, Qt::SolidLine, Qt::SquareCap, Qt::BevelJoin));

    bool fix = true;
    for (int i = 0; i < 4 ; i++)
    {
        outputs.LL = &ui->video_feed->posLeftDown[i];
        outputs.LR = &ui->video_feed->posRightDown[i];
        outputs.UL = &ui->video_feed->posLeftUp[i];
        outputs.UR = &ui->video_feed->posRightUp[i];

        if (fix)
        {
          //  fix = false;
            IR_Harris(imageData, i, outputs);
         //   updateROI(&outputs.cThm.c[i], outputs.UL, outputs.UR, outputs.LL, outputs.LR, true);
        }
        qPainter.drawRect(outputs.cThm.c[i].x0-5, outputs.cThm.c[i].x1-5, 10, 10);
    }

//    if (frames < 3)
//    {
//        outputs.LL = ui->video_feed->posLeftDown;
//        outputs.LR = ui->video_feed->posRightDown;
//        outputs.UL = ui->video_feed->posLeftUp;
//        outputs.UR = ui->video_feed->posRightUp;
//    }

    //for (int i = 0; i < 4; i++)
    {/*
        //qDebug() << "ñaa" << outputs.LL[i]
                          << outputs.LR[i]
                          << outputs.UL[i]
                          << outputs.UR[i]
                          << outputs.cThm.c[i].x0
                          << outputs.cThm.c[i].x1;*/
    }


//    for (int num = 0; num < 4; num++)
//    {
//         updateROI(&outputs.cThm.c[num], outputs.UL, outputs.UR, outputs.LL, outputs.LR, true);
//       // updateROI(&outputs.cThm.c[num], &outputs.UL[num], &outputs.UR[num], &outputs.LL[num], &outputs.LR[num], true);
//       // updateRoiValues(corners_thm, num, num+1, &outputs.UR[num], &outputs.LR[num], &outputs.UL[num], &outputs.LL[num]);
//    }

//    for (int i = 0; i < 4; i++)
//    {
//        //qDebug() << "ñee" << outputs.LL[i]
//                          << outputs.LR[i]
//                          << outputs.UL[i]
//                          << outputs.UR[i]
//                          << outputs.cThm.c[i].x0
//                          << outputs.cThm.c[i].x1;
//    }

    // QPixmap qim = QPixmap(ui->video_feed->Pixmap());


//    for (int i = 0; i < 4; i++)
//    {
//        //circle(matCv_scaled, Point(outputs.cThm.c[i].x0, outputs.cThm.c[i].x1), 5, Scalar(0), 2, 8, 0);
//        qPainter.drawRect(outputs.cThm.c[i].x0-5, outputs.cThm.c[i].x1-5, 10, 10);
//    }

    qPainter.end();

    ui->video_feed->label->setPixmap(raro);
    tempVals = ImageRegistration(saveIR, matRGB.data, pts_src, pts_dst);
    //ImageRegistrationTEST(showIR, matRGB.data, pts_src, pts_dst);
}

//Needs:
// - A infrared video frame.
// - The positions of the ROI in an structure.
// - A boolean value indicating if the video has ended.
IRvideoOutputs MainWindow::getIRframeInfo(uchar showIR[FRAME_TAU_WIDTH*FRAME_TAU_HEIGHT*3], IRHarrisOutputs outputs)
{
    for (int num = 0; num < 4; num++)
    {
        IR_Harris(showIR, num, outputs);
        //updateRoiValues(outputs.cThm, num, num+1, outputs.UR, outputs.LR, outputs.UL, outputs.LL);
        updateROI(&outputs.cThm.c[num], &outputs.UL[num], &outputs.UR[num], &outputs.LL[num], &outputs.LR[num], true);
    }

     temps temperatureValues = ImageRegistration(saveIR, matRGB.data, pts_src, pts_dst);

     IRvideoOutputs cornerAndTemp;
     cornerAndTemp.cThm = outputs.cThm;
     cornerAndTemp.t = temperatureValues;

     return cornerAndTemp;
}
void MainWindow::updateRoiValues(CORNERS corners_thm, int it, int enter, QPoint * UpperRight, QPoint * LowerRight, QPoint * UpperLeft, QPoint * LowerLeft)
{
    for (int num = 0; num < it; num++)
    {
        (it == 1) ? updateROI(&corners_thm.c[enter], &UpperLeft[enter], &UpperRight[enter], &LowerLeft[enter], &LowerRight[enter], false) :  \
                    updateROI(&corners_thm.c[num], &UpperLeft[num], &UpperRight[num], &LowerLeft[num], &LowerRight[num], false);
    }
}
temps MainWindow::ImageRegistration(uint16_t* uiIR, uint8_t* uiProy, vector<Point2f> corner_src, vector<Point2f> corner_dst)
{
    QElapsedTimer tim;
    tim.start();

    CORNERS c_proy;

    if (frames == 2)
        ui->plainTextEdit->insertPlainText("Registering images");
//   //qDebug() << corner_src.size() << corner_dst.size();

    Mat fH = findHomography(corner_src, corner_dst);

    theta[0] = double (float (fH.at<double>(0,0)));
    theta[1] = double (float (fH.at<double>(0,1)));
    theta[2] = double (float (fH.at<double>(0,2)));
    theta[3] = double (float (fH.at<double>(1,0)));
    theta[4] = double (float (fH.at<double>(1,1)));
    theta[5] = double (float (fH.at<double>(1,2)));
    theta[6] = double (float (fH.at<double>(2,0)));
    theta[7] = double (float (fH.at<double>(2,1)));
    theta[8] = double (float (fH.at<double>(2,2)));

    if (firstframe)
    {
        avgTempLesion = apply_proy_tform_mask(uiIR, uiProy, true, theta, &lesionList, &c_proy, c_assoc);
        avgTempSkin = apply_proy_firstframe(uiIR, uiProy, false, theta, &skinList, &newMask, avgTempLesion);
        firstframe = false;
    }

     avgTempLesion = apply_proy_tform_mask(uiIR, uiProy, true, theta, &lesionList, &c_proy, c_assoc);
     avgTempSkin = apply_proy_tform_mask(uiIR, uiProy, false, theta, &newMask, &c_proy, c_assoc);

    QImage imgReg = QImage((uint8_t*) uiProy, FRAME_TAU_WIDTH, FRAME_TAU_HEIGHT, FRAME_TAU_WIDTH, QImage::Format_Grayscale8);
    ui->image->setPixmap(QPixmap::fromImage(imgReg));

    ui->video_feed->setPixmap(raro);

    temps tempsInandOut;

    //Save all values for classification
    skin.append(avgTempSkin);
    lesion.append(avgTempLesion);

    tempsInandOut.tempLesion = avgTempLesion;
    tempsInandOut.tempSurround = avgTempSkin;

    ////qDebug() << tempsInandOut.tempLesion << tempsInandOut.tempSurround;


    double error = 0;

    for (int i = 0; i < 4; i++)
    {
        qDebug() << c_proy.c[i].x1 << outputs.cThm.c[i].x1
        << c_proy.c[i].x0 << outputs.cThm.c[i].x0 ;

        res[i] = (c_proy.c[i].x1 - outputs.cThm.c[i].x1)*(c_proy.c[i].x1 - outputs.cThm.c[i].x1) +
                 (c_proy.c[i].x0 - outputs.cThm.c[i].x0)*(c_proy.c[i].x0 - outputs.cThm.c[i].x0) ;

        error = (error + res[i]);

       //qDebug() << sqrt(res[i]);
//       if (sqrt(res[i]) > 10)
//       {
//           ui->error->setStyleSheet("QPushButton {background-color: red}");
//           ui->error->setText("At least one corner is qrong : " + QString::number(sqrt(res[i])) + " pixels");
//       }

    }

    error = sqrt(error)/4;

    ui->error->setText("Error : " + QString::number(error) + " pixels");

    if (error > 5)
        ui->error->setStyleSheet("QPushButton {background-color: red}");


    ////qDebug() << "RES" << error;

    return tempsInandOut;
}


////////////////////////////////////////////////////////////////////////////////////////
/**************************** CAMARA IR RECEIVER - JAVIER *****************************/
////////////////////////////////////////////////////////////////////////////////////////

void MainWindow::newFrame (quint8 * frame)
{
    uchar FrameIMG[512][640];

    memcpy ( FrameIMG, frame, 640*512 );

    QImage IMG = QImage(*FrameIMG, 640, 512, QImage::Format_Indexed8);
    ui->image->setPixmap(QPixmap::fromImage(IMG));

}


/*
void MainWindow::ReadUDP_conc(){
    QFuture<void> future = QtConcurrent::run(this, &MainWindow::ReadUDP);
    future.waitForFinished();
}

void MainWindow::ReadUDP()
{
    gettimeofday(&newcall, NULL);
    calltime = (newcall.tv_sec - oldcall.tv_sec)*1000000 + (newcall.tv_usec - oldcall.tv_usec);
    oldcall = newcall;
    //qDebug() << "Call time:\t" << calltime << "\t[us]";

    gettimeofday(&startTime, NULL);

    QByteArray buffer;
    buffer.resize(socket->pendingDatagramSize());
    // Solicita paquete ethernet a buffer
    socket->readDatagram(buffer.data(), buffer.size());
    // Fila correspondiente en frame
    int row = (quint8(buffer[1281]) << 8) | quint8(buffer[1280]);

    if(row == 600)
    {
        QMessageBox::warning(this, tr("Error"), tr("Transmission errors, try again"), QMessageBox::Ok);
        return;
    }

    if(row > 512) return;

    memcpy((uchar*)(&FrameTau[row][0]), (uchar*)buffer.data(), 640*2);

    // Minimos y maximos para auto escala
    quint16 maximum = 0;
    quint16 minimum = 0xFFFF;

    bool autoscale = ui->autoscale->isChecked();

    // Comprobacion de que se recibe el frame completo
    line_counter++;
    if(row == 0) line_counter = 0;


//    if ( row != line_counter )
//        //qDebug() << "Error"  << row << line_counter;

    // Se completo frame
    if ((row == 511 && line_counter == 511) || row != line_counter )
    {
        //qDebug()<< "Llego frame entero";
        //ui->plainTextEdit->insertPlainText("llego frame IR.\n");
        //ui->plainTextEdit->verticalScrollBar()->setValue(ui->plainTextEdit->verticalScrollBar()->maximum());
        line_counter = row;
        // Minimos y maximos de auto rango
        if(autoscale)
        {
            for (int j = 0; j < 512; ++j)
            {
                for (int i = 0; i < 640; ++i)
                {
                    int mean = (last_min + last_max) / 2;
                    if (FrameTau[j][i] < 0.5*mean) FrameTau[j][i] = mean;
                    if (FrameTau[j][i] > mean/0.5) FrameTau[j][i] = mean;

                    if (FrameTau[j][i] > maximum) maximum = FrameTau[j][i];
                    if (FrameTau[j][i] < minimum) minimum = FrameTau[j][i];
                }
            }

            last_min = minimum;
            last_max = maximum;
        }

        // Copia imagen
        for (int j = 0; j < 512; ++j)
        {
            #pragma omp parallel for //PARARELO
            for (int i = 0; i < 640; ++i)
            {
                FrameIMG[j][639-i]  = (255*(FrameTau[j][i] - last_min)/(last_max - last_min)) & 0xFF;
                FrameTau2[j][639-i] = FrameTau[j][i];
            }
        }

        cv::Mat mat(512, 640, CV_8U, FrameIMG);

        cv::Mat heatmap;
        cv::applyColorMap( mat, heatmap, cv::COLORMAP_PINK );

        // Muestra imagen en interfaz
        QImage imgIn= QImage((uchar*) heatmap.data, heatmap.cols, heatmap.rows, heatmap.step, QImage::Format_RGB888);
        ui->image->setPixmap(QPixmap::fromImage(imgIn));

         //Almacena cuadro en video en caso de estar grabando
        if(RecordVideo)
        {
            OutputVideo.write(mat);
        }
    }
    gettimeofday(&endTime, NULL);
    micros = (endTime.tv_sec - startTime.tv_sec)*1000000 + (endTime.tv_usec - startTime.tv_usec);
    //qDebug() <<"ReadUDP time:\t" << micros << "\t[us]\t";

    if(micros < calltime){
        //qDebug() << "OK";
        //ui->plainTextEdit->insertPlainText("Ok.\n");
        //ui->plainTextEdit->verticalScrollBar()->setValue(ui->plainTextEdit->verticalScrollBar()->maximum());
    }
    else{
        //qDebug() << "PROBLEMA";
        //ui->plainTextEdit->insertPlainText("Problema.\n");
        //ui->plainTextEdit->verticalScrollBar()->setValue(ui->plainTextEdit->verticalScrollBar()->maximum());
    }
}

void MainWindow::setStream(bool on)
{
    QByteArray Data;
    Data[0] = 0xD0;
    Data[1] = 0xDF;
    Data[2] = on;
    socket->writeDatagram(Data, IPDest, 1234);
    Data.clear();
}

void MainWindow::on_autoscale_clicked()
{
    if( !ui->autoscale->isChecked() )
    {
        last_min = 0;
        last_max = 3000;
    }
}

/*************************************/

void MainWindow::on_quit_clicked()
{
    QApplication::quit();
}


void MainWindow::on_pushButton_clicked()
{
    bool ok;
   // seg_rang= QInputDialog::getInt(this,tr("Ingresar segundos: "),tr("Segundos:"),
   //                                    120,0,1000,1, &ok );
    seg_rang1= QInputDialog::getInt(this,tr("Ingresar eje Y:"),tr("From:"),
                                       0,0,1000,1, &ok );
    seg_rang2= QInputDialog::getInt(this,tr("Ingresar eje Y:"),tr("To:"),
                                       20,0,1000,1, &ok );
    ui->plot1->xAxis->setRange(0, seg_rang);
    ui->plot1->yAxis->setRange(seg_rang1, seg_rang2);
    ui->plot1->replot();

}

void MainWindow::on_pushButton_2_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(NULL,
                                        tr("Open File"),  QDir::currentPath());

    QImage image(fileName);
    ui->image->setPixmap(QPixmap::fromImage(image));
    //ui->image->show();
}

void MainWindow::on_segmentation_2_clicked(){}

void MainWindow::on_reg_clicked()
{
    timerReg->start(1000/8);

   // frameTimer->start();

    ui->reg->setStyleSheet("QPushButton {background-color: lightblue}");
}

/************************************************************************************************/
/************************************************************************************************/


void MainWindow::ImageRegistrationTEST(uint8_t* uiIR, uint8_t* uiProy, vector<Point2f> corner_src, vector<Point2f> corner_dst)
{
    QElapsedTimer tim;
    tim.start();

    Mat fH = findHomography(corner_dst, corner_src);

    theta[0] = double (float (fH.at<double>(0,0)));
    theta[1] = double (float (fH.at<double>(0,1)));
    theta[2] = double (float (fH.at<double>(0,2)));
    theta[3] = double (float (fH.at<double>(1,0)));
    theta[4] = double (float (fH.at<double>(1,1)));
    theta[5] = double (float (fH.at<double>(1,2)));
    theta[6] = double (float (fH.at<double>(2,0)));
    theta[7] = double (float (fH.at<double>(2,1)));
    theta[8] = double (float (fH.at<double>(2,2)));

    for (int i = 0; i < FRAME_TAU_HEIGHT; i++)
    {
        apply_proy_tform_line(uiProy, uiIR, &c_assoc.cm, &outputs.cThm.cm, theta, i);
    }

    QImage imgReg = QImage((uint8_t*) uiIR, FRAME_TAU_WIDTH, FRAME_TAU_HEIGHT, FRAME_TAU_WIDTH, QImage::Format_Grayscale8);
    ui->video_feed->setPixmap(QPixmap::fromImage(imgReg));

}


//void MainWindow::on_gainSlider_valueChanged(int value)
//{
//    gainCal = (double)value/32768;
//    ui->gain->setText(QString("%1").arg(gainCal));
//}

//void MainWindow::on_offsetSlider_valueChanged(int value)
//{
//    offsetCal = value;
//    ui->offset->setText(QString("%1").arg(offsetCal));
//}
