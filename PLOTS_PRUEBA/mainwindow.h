#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QRgb>
#include <QVector>
#include <vector>
#include <QTime>
#include <QPainter>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
//#include <opencv2/videoio.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <registration.h>

using namespace std;
using namespace cv;

#define ROW 480
#define COL 640

//IRCAM
#include <QUdpSocket>
#include <QFileDialog>
#include <QNetworkInterface>
#include <QThread>
#include <QImageWriter>
#include <QScrollBar>
#include <QMessageBox>
#include <segmentation.h>
#include <registration.h>
#include <QElapsedTimer>
#include <sys/time.h>
#include "thermapp.h"

#define NFEATURES           100

namespace Ui {
class MainWindow;
}

class QMenu;
class QAction;


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void addPoint(double x, double y, double x2, double y2);
    void plot();
    void clearData();
    QPixmap qim;
    QImage qt_image;
    QImage imgFrame;
    QImage qt_image_copy;
     uchar frame_trans[PIXELS_DATA_SIZE];
    vector<Point2f> pts_src, pts_dst;
    QPainter qPainter;
    int seg_param;
    int seg_rang = 0;
    int seg_rang1 = 0;
    int seg_rang2 = 0;
    QList<float> lesion;
    QList<float> skin;

    double ganancia[PIXELS_DATA_SIZE];
    double offset[PIXELS_DATA_SIZE];

    uchar matIR[FRAME_TAU_HEIGHT*FRAME_TAU_WIDTH];


    uchar data[PIXELS_DATA_SIZE] ;

    double res[4]= {0,0,0,0};

    bool cornerFlag = true;

    //ThermApp
    ThermApp *therm;
    QTimer *frameTimer;
    uchar imageData[PIXELS_DATA_SIZE*3];
    uchar frame2[PIXELS_DATA_SIZE];
    short image_cal[PIXELS_DATA_SIZE]; //Calibrate data
    float temp_prev;

    int DCoffset;
    short pixel0;

    //FILE *wfp;

   QTimer *sTimer;

    float Tambient;
    short PixL[11], PixM[11], PixH[11];
    short PixL_,PixM_,PixH_;
    int count;

    double gainCal;
    int offsetCal;


    bool calib = false;


public slots:
    void newFrame (quint8 *frame);


private slots:

    void updateVideo();

    CORNERS RGB_Harris(QImage& img, IRHarrisOutputs positionsROI);

    void generateMask(QImage img, CORNERS cRGB);

    maskLists clickSegm(int posx, int posy);

    //IRHarrisOutputs IR_Harris(uint8_t showIR[FRAME_TAU_WIDTH*FRAME_TAU_HEIGHT], int cornerID, IRHarrisOutputs outputs);

    //CORNERS firstFrame(uint8_t showIR[FRAME_TAU_WIDTH*FRAME_TAU_HEIGHT], int cornerID, IRHarrisOutputs outputs);

    //IRvideoOutputs getIRframeInfo(uint8_t showIR[FRAME_TAU_WIDTH*FRAME_TAU_HEIGHT], IRHarrisOutputs outputs, maskLists lists);

    //IRvideoOutputs classify(maskLists lists);

    //void ImageRegistration(uint16_t* uiIR, uint8_t* uiProy, vector<Point2f> corner_src, vector<Point2f> corner_dst);

    //void regFirstFrame();

    //void updateVideo();

    //void getVideo();

    void getImage(QImage& image, uchar matRGBFull[ROW][COL]);

    void realtimeDataSlot();

    void on_action_Save_As_triggered();

    void on_restart_clicked();

    void on_replot_clicked();

    void on_saveplot_clicked();

    void on_open_clicked();

    void update_window();

    void on_close_clicked();

    void on_snap_clicked();

    void on_stop_clicked();

    void on_addData_clicked();

    void on_redraw_clicked();

    void pinta_cuads(int);

    void esquinas();

    // IRCAM
   // void ReadUDP();
    //void setStream(bool on);
    //void on_autoscale_clicked();
    //void ReadUDP_conc();

    void on_quit_clicked();

    void on_segmentation_clicked();

    void get_otsuclick_pos(QPoint click);

    void on_startIR_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void onSegmReady();

    void getVideo();

    void IRframes();



    void on_segmentation_2_clicked();

    void on_reg_clicked();

    QPixmap Frame(short *frame);
    void UpdateProcessFrame();

    void on_gainSlider_valueChanged(int value);

    void on_offsetSlider_valueChanged(int value);


private:
    Ui::MainWindow *ui;

    /*****************************************************************************************************************/

    void ImageRegistrationTEST(uint8_t* uiIR, uint8_t* uiProy, vector<Point2f> corner_src, vector<Point2f> corner_dst);

    /*****************************************************************************************************************/

    void updateRoiValues(CORNERS corners_thm, int it, int enter, QPoint * UpperRight, QPoint * LowerRight, QPoint * UpperLeft, QPoint * LowerLeft);

    IRvideoOutputs getIRframeInfo(uint8_t showIR[FRAME_TAU_WIDTH*FRAME_TAU_HEIGHT], IRHarrisOutputs outputs);

    void IR_Harris(uchar showIR[FRAME_TAU_WIDTH*FRAME_TAU_HEIGHT*3], int cornerID, IRHarrisOutputs &outputs);

    CORNERS firstFrame(uchar showIR[FRAME_TAU_WIDTH*FRAME_TAU_HEIGHT*3], int cornerID, IRHarrisOutputs outputs);

    temps ImageRegistration(uint16_t* uiIR, uint8_t* uiProy, vector<Point2f> corner_src, vector<Point2f> corner_dst);

    QVector<double> qv_x, qv_y, qv_x2, qv_y2;
    QVector<int> red,green,blue;
    cv::VideoCapture cap;
    cv::Mat frame, frame_rgb;
    QTimer *timer, *dataTimer;
    temps tempVals;

    bool otsuReady = false;
    /************************/
    QImage img;

    QTime frameTime;

    QDataStream * inIR;
    int frames = 0;

    int i=1,j=1;
    int framecount=0;
    int camWidth=FRAME_TAU_WIDTH, camHeight=FRAME_TAU_HEIGHT;
    double fps=0;

    QString filename;
    bool videoOver;
    QTimer * timerReg;

    bool IR = false;
    bool firstframe = true;
    bool firstReg = true;

    bool second_video=false;

    QPoint posRightUp[4];
    QPoint posRightDown[4];
    QPoint posLeftUp[4];
    QPoint posLeftDown[4];

    IRHarrisOutputs outputs;
    QPixmap raro;

    uchar mask_final[ROWROI][COLROI];

    // uint8_t * mask_final;
    int miniRow;
    int min_corner_x;
    int min_corner_y;

    int max_corner_x;
    int max_corner_y;
    int miniCol;
    int seeds[2];
    int clicknum;

    int posx=0,posy=0; //click otsu
    IRHarrisOutputs posROIs;
    CORNERS esq;
    CORNERS cThm;

    uint8_t * matErode;
    uint8_t * matDilate;
    Mat matComp;
    Mat dstMask;
    QList<QPoint> lesionList;
    QList<QPoint> skinList;
    QList<QPoint> newMask;

    QList<float> allLesionValues;
    QList<float> allSkinValues;

    float avgTempLesion;
    float avgTempSkin;
    Mat matOutSeg;

    //int frame = 0;

    uint8_t* uiProyPre;

    QUdpSocket *socket;
    QUdpSocket *socket2;
    quint16 last_min, last_max;
    int line_counter;
//    quint16 FrameTau[512][640];
//    quint16 FrameTau2[512][640];
//    uchar FrameIMG[512][640];
    cv::VideoWriter OutputVideo;
    bool RecordVideo;
    bool DatabaseMode;
    bool StreamOn;
    int PhotoCounter;

    QString folderName;

    QHostAddress IPDest;
    QElapsedTimer udp_timer;

    struct timeval startTime, endTime, oldcall, newcall;
    unsigned int micros, calltime;

};

#endif // MAINWINDOW_H
