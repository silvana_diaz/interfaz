#ifndef CLASSIFICATION_H
#define CLASSIFICATION_H

#include <stdint.h>
#include <QPoint>
#include <QList>

QString classification (QList<float> lesion, QList<float> skin, QString algorithm);
QVector<float> getLambda(QString lamval);
void fitData (QList<float> values, float param[5]);
void getPsi(float psi[7000][12], QString psival);

#endif // CLASSIFICATION_H
