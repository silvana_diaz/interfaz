/****************************************************************************
** Meta object code from reading C++ file 'taunuc.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "taunuc.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'taunuc.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TauNUC_t {
    QByteArrayData data[14];
    char stringdata0[104];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TauNUC_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TauNUC_t qt_meta_stringdata_TauNUC = {
    {
QT_MOC_LITERAL(0, 0, 6), // "TauNUC"
QT_MOC_LITERAL(1, 7, 8), // "finished"
QT_MOC_LITERAL(2, 16, 0), // ""
QT_MOC_LITERAL(3, 17, 5), // "error"
QT_MOC_LITERAL(4, 23, 3), // "err"
QT_MOC_LITERAL(5, 27, 14), // "processedFrame"
QT_MOC_LITERAL(6, 42, 7), // "quint8*"
QT_MOC_LITERAL(7, 50, 7), // "process"
QT_MOC_LITERAL(8, 58, 8), // "newFrame"
QT_MOC_LITERAL(9, 67, 8), // "quint16*"
QT_MOC_LITERAL(10, 76, 5), // "frame"
QT_MOC_LITERAL(11, 82, 9), // "setParams"
QT_MOC_LITERAL(12, 92, 4), // "gain"
QT_MOC_LITERAL(13, 97, 6) // "offset"

    },
    "TauNUC\0finished\0\0error\0err\0processedFrame\0"
    "quint8*\0process\0newFrame\0quint16*\0"
    "frame\0setParams\0gain\0offset"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TauNUC[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x06 /* Public */,
       3,    1,   45,    2, 0x06 /* Public */,
       5,    1,   48,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    0,   51,    2, 0x0a /* Public */,
       8,    1,   52,    2, 0x0a /* Public */,
      11,    2,   55,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void, 0x80000000 | 6,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, 0x80000000 | 9, 0x80000000 | 9,   12,   13,

       0        // eod
};

void TauNUC::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TauNUC *_t = static_cast<TauNUC *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->finished(); break;
        case 1: _t->error((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->processedFrame((*reinterpret_cast< quint8*(*)>(_a[1]))); break;
        case 3: _t->process(); break;
        case 4: _t->newFrame((*reinterpret_cast< quint16*(*)>(_a[1]))); break;
        case 5: _t->setParams((*reinterpret_cast< quint16*(*)>(_a[1])),(*reinterpret_cast< quint16*(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (TauNUC::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TauNUC::finished)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (TauNUC::*_t)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TauNUC::error)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (TauNUC::*_t)(quint8 * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TauNUC::processedFrame)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject TauNUC::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_TauNUC.data,
      qt_meta_data_TauNUC,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *TauNUC::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TauNUC::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TauNUC.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int TauNUC::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void TauNUC::finished()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void TauNUC::error(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void TauNUC::processedFrame(quint8 * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
